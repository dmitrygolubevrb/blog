<?php


namespace App\Repositories;


use App\Models\User;
use App\Repositories\Interfaces\QueryRepositoryInterface;
use Spatie\QueryBuilder\QueryBuilder;

class UserRepository implements QueryRepositoryInterface
{

    public function getByQuery()
    {
        return QueryBuilder::for(User::class)
            ->allowedFilters(['email'])
            ->get();
    }
}
