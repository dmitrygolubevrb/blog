<?php


namespace App\Repositories;


use App\Models\Post;
use App\Repositories\Interfaces\PostRepositoryInterface;
use App\Repositories\Interfaces\QueryRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\QueryBuilder;

class PostRepository implements QueryRepositoryInterface, PostRepositoryInterface
{

    public function getByQuery()
    {
        return QueryBuilder::for(Post::class)
            ->allowedFilters(['title', 'user_id', 'created_at', 'updated_at'])
            ->allowedSorts('created_at')
            ->get();
    }

    public function index()
    {
        $posts = DB::table('posts')
            ->select(DB::raw('t2.*'))
            ->from(DB::raw('(select category_id from posts group by category_id) as t1,
            lateral (select posts.*, images.url, images.preview_url, categories.title as category_title
            from posts, images, categories
            where t1.category_id=posts.category_id AND posts.id=images.post_id AND posts.category_id=categories.id
            order by posts.created_at desc limit 10) as t2'))
            ->get();

        $postsByCategories = [];
        foreach ($posts as $post) {
            $post->created_at = Carbon::parse($post->created_at)->diffForHumans();
            $postsByCategories[$post->category_title][] = $post;
        }
        return $postsByCategories;
    }
}
