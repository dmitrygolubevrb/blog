<?php


namespace App\Repositories\Interfaces;


interface QueryRepositoryInterface
{
    public function getByQuery();
}
