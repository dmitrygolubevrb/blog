<?php

namespace App\Providers;

use App\Http\Controllers\Admin\Post\IndexController;
use App\Repositories\BlogRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\Interfaces\BlogRepositoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\QueryRepositoryInterface;
use App\Repositories\Interfaces\TagRepositoryInterface;
use App\Repositories\PostRepository;
use App\Repositories\TagRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CategoryRepositoryInterface::class,
            CategoryRepository::class
        );
        $this->app->bind(
            TagRepositoryInterface::class,
            TagRepository::class
        );

        $this->app->when(IndexController::class)
            ->needs(QueryRepositoryInterface::class)
            ->give(function () {
                return PostRepository::class;
            });

        $this->app->when(\App\Http\Controllers\Admin\User\IndexController::class)
            ->needs(QueryRepositoryInterface::class)
            ->give(function () {
                return UserRepository::class;
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
