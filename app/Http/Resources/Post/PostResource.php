<?php

namespace App\Http\Resources\Post;

use App\Http\Resources\Author\AuthorResource;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Image\ImageResource;
use App\Http\Resources\Tag\TagResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'content_preview' => $this->content_preview,
            'category' => new CategoryResource($this->category),
            'created_at' => Carbon::parse($this->created_at)->diffForHumans(),
            'tags' => TagResource::collection($this->tags),
            'image' => new ImageResource($this->image),
            'author' => new AuthorResource($this->author),
            'comments_count' => $this->comments_count,
            'likes_count' => $this->likes_count,
            'view_count' => $this->view_count
        ];
    }
}
