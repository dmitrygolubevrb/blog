<?php

namespace App\Http\Controllers\Category\Post;

use App\Http\Controllers\Controller;
use App\Http\Resources\Post\PostResource;
use App\Models\Category;
use Illuminate\Http\Request;
use Psy\Util\Str;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke(Category $category)
    {
        return PostResource::collection($category->posts);
    }
}
