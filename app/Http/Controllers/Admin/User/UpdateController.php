<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\UpdateRequest;
use App\Models\User;
use App\Services\UserService;

class UpdateController extends Controller
{

    public function __invoke(UpdateRequest $request, User $user, UserService $userService)
    {
        return $userService->update($request, $user);
    }

}
