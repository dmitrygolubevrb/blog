<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\StoreRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Services\UserService;

class StoreController extends Controller
{

    public function __invoke(StoreRequest $request, UserService $userService)
    {
            $result = $userService->store($request);
            return $result instanceof User ? new UserResource($result) : $result;
    }
}
