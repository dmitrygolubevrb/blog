<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;

class DestroyController extends Controller
{
    public function __invoke(User $user, UserService $userService)
    {
        $userService->destroy($user);
        return response([]);
    }
}
