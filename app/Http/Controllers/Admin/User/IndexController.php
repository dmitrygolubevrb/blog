<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\Repositories\Interfaces\QueryRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;

class IndexController extends Controller
{
    public function __invoke(QueryRepositoryInterface $userRepository)
    {
        return UserResource::collection($userRepository->getByQuery());
    }
}
