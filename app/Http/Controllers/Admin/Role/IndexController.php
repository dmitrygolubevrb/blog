<?php

namespace App\Http\Controllers\Admin\Role;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**Get Roles
     * @return array
     */
    public function __invoke(): array
    {
        return ['roles' => User::getRoles()];
    }
}
