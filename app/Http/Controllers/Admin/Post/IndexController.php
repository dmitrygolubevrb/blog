<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;
use App\Repositories\Interfaces\QueryRepositoryInterface;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke(QueryRepositoryInterface $postRepository)
    {
        return PostResource::collection($postRepository->getByQuery());
    }
}
