<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Services\CategoryService;

class StoreController extends Controller
{


    /**
     * @param StoreRequest $request
     * Handle the incoming request.
     */
    public function __invoke(StoreRequest $request, CategoryService $categoryService)
    {
        return new CategoryResource($categoryService->store($request));
    }
}
