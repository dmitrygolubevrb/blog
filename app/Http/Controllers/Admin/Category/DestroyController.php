<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Services\CategoryService;

class DestroyController extends Controller
{

    public function __invoke(Category $category, CategoryService $categoryService)
    {
        $categoryService->destroy($category);
        return response([]);
    }
}
