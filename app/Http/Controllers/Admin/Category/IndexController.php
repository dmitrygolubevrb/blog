<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Http\Resources\Category\CategoryResource;
use App\Repositories\Interfaces\CategoryRepositoryInterface;

class IndexController extends Controller
{

    public function __invoke(CategoryRepositoryInterface $categoryRepository)
    {
        return CategoryResource::collection($categoryRepository->all());
    }

}
