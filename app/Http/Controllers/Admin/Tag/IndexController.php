<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Http\Controllers\Controller;
use App\Http\Resources\Tag\TagResource;
use App\Repositories\Interfaces\TagRepositoryInterface;

class IndexController extends Controller
{

    public function __invoke(TagRepositoryInterface $tagRepository)
    {
        return TagResource::collection($tagRepository->all());
    }
}
