<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use App\Services\TagService;

class DestroyController extends Controller
{

    public function __invoke(Tag $tag, TagService $tagService)
    {
       $tagService->destroy($tag);
        return response([]);
    }
}
