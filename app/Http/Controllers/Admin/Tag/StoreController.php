<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tag\StoreRequest;
use App\Http\Resources\Tag\TagResource;
use App\Services\TagService;

class StoreController extends Controller
{


    public function __invoke(StoreRequest $request, TagService $tagService)
    {
        return new TagResource($tagService->store($request));
    }
}
