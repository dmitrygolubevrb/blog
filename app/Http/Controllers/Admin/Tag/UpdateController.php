<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tag\UpdateRequest;
use App\Models\Tag;
use App\Services\TagService;

class UpdateController extends Controller
{
    /**
     * @param UpdateRequest $request
     * @param Tag $tag
     */
    public function __invoke(UpdateRequest $request, Tag $tag, TagService $tagService)
    {
        return $tagService->update($request, $tag);
    }
}
