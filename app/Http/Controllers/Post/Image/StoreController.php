<?php

namespace App\Http\Controllers\Post\Image;

use App\Http\Controllers\Controller;
use App\Http\Requests\Image\StoreRequest;
use App\Services\ImageService;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request, ImageService $imageService)
    {
        return response()->json($imageService->store($request));
    }
}
