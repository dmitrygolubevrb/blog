<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\UpdateRequest;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;
use App\Services\PostService;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Post $post, PostService $postService)
    {
        $result = $postService->update($request, $post);
        return $result instanceof Post ? new PostResource($result) : $result;
    }
}
