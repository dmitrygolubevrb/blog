<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Resources\Post\PostResource;
use App\Repositories\PostRepository;

class IndexController extends Controller
{
    public function __invoke(PostRepository $postRepository)
    {
        return PostResource::collection($postRepository->getByQuery());
    }
}
