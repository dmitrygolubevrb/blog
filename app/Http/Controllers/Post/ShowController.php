<?php

namespace App\Http\Controllers\Post;

use App\Events\PostHasViewed\PostHasViewed;
use App\Http\Controllers\Controller;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;

class ShowController extends Controller
{

    public function __invoke(Post $post)
    {
        PostHasViewed::dispatch($post);
        return new PostResource($post);
    }
}
