<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Services\PostService;
use Illuminate\Http\Request;

class DestroyController extends Controller
{
    public function __invoke(Post $post, PostService $postService)
    {
        $postService->destroy($post);
        return response([]);
    }
}
