<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\StoreRequest;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;
use App\Services\PostService;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request, PostService $postService)
    {

        $result = $postService->store($request);
        return $result instanceof Post ? new PostResource($result) : $result;
    }
}
