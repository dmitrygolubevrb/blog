<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\BlogRepositoryInterface;

class IndexController extends Controller
{

    public function __invoke(BlogRepositoryInterface $blogRepository)
    {
        return $blogRepository->index();
    }

}
