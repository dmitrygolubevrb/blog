<?php


namespace App\Http\Services\User;


use App\Mail\User\PasswordMail;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class Service
{

    public function store($data)
    {

    }

    public function update($data, $user)
    {
        return $user->update($data);
    }

}
