<?php


namespace App\Services;


use App\Http\Requests\Post\StoreRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Models\Image;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;

class PostService
{
    public function store(StoreRequest $request)
    {
        try {
            $data = $request->validated();
            DB::beginTransaction();
            $mainImage = $data['main_image'];
            if (Arr::exists($data, 'tags_ids')) {
                $tagsIds = $data['tags_ids'];
                unset($data['tags_ids']);
            }
            unset($data['main_image']);

            $imageName = md5(Carbon::now() . $mainImage->getClientOriginalName())
                . '.' . $mainImage->getClientOriginalExtension();
            $previewImageName = 'preview_' . $imageName;

            InterventionImage::make($mainImage)
                ->fit(1200, 630)
                ->save(storage_path('app/public/images/' . $previewImageName), 90);
            $mainImagePath = Storage::disk('public')
                ->putFileAs('/images', $mainImage, $imageName);

            $data['user_id'] = auth()->user()->id;
            $post = Post::create($data);
            Image::create([
                'path' => $mainImagePath,
                'url' => url('/storage/' . $mainImagePath),
                'preview_url' => url('/storage/images/' . $previewImageName),
                'post_id' => $post->id
            ]);
            if (isset($tagsIds)) {
                $post->tags()->attach($tagsIds);
            }

            DB::commit();

            return $post;

        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception->getMessage();
        }
    }

    public function update(UpdateRequest $request, Post $post)
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            if (isset($data['main_image'])) {
                $mainImage = $data['main_image'];
                $imageName = md5(Carbon::now() . $mainImage->getClientOriginalName())
                    . '.' . $mainImage->getClientOriginalExtension();
                $previewImageName = 'preview_' . $imageName;
                InterventionImage::make($mainImage)
                    ->fit(1200, 630)
                    ->save(storage_path('app/public/images/' . $previewImageName), 90);
                $mainImagePath = Storage::disk('public')
                    ->putFileAs('/images', $mainImage, $imageName);
            }
            unset($data['main_image']);


            $post->tags()->sync($data['tags_ids']);

            unset($data['tags_ids']);

            $post->update($data);
            if (isset($mainImage)) {
                $post->image->update([
                    'path' => $mainImagePath,
                    'url' => url('/storage/' . $mainImagePath),
                    'preview_url' => url('/storage/images/' . $previewImageName),
                    'post_id' => $post->id
                ]);
            }

            DB::commit();

            return $post->fresh();

        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception->getMessage();
        }
    }

    public function destroy(Post $post)
    {
        $post->tags()->detach();
        $post->delete();
    }
}
