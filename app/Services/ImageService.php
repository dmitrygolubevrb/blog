<?php


namespace App\Services;



use App\Http\Requests\Image\StoreRequest;
use Illuminate\Support\Facades\Storage;

class ImageService
{
    public function store(StoreRequest $request)
    {
        $path = Storage::disk('public')->putFile('images', $request->image);
        return ['url' => url('storage/' . $path)];
    }
}
