<?php


namespace App\Services;


use App\Http\Requests\Admin\Category\StoreRequest;
use App\Http\Requests\Admin\Category\UpdateRequest;
use App\Models\Category;

class CategoryService
{

    public function destroy(Category $category)
    {
        $category->delete();
    }

    public function store(StoreRequest $request)
    {
       return Category::firstOrCreate($request->validated());
    }

    public function update(UpdateRequest $request, Category $category)
    {
        return $category->update($request->validated());
    }

}
