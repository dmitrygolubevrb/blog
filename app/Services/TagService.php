<?php


namespace App\Services;


use App\Http\Requests\Admin\Tag\StoreRequest;
use App\Http\Requests\Admin\Tag\UpdateRequest;
use App\Models\Tag;

class TagService
{

    public function destroy(Tag $tag)
    {
        $tag->delete();
    }

    public function store(StoreRequest $request)
    {
        return Tag::firstOrCreate($request->validated());
    }

    public function update(UpdateRequest $request, Tag $tag)
    {
        return $tag->update($request->validated());
    }

}
