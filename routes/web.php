<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'auth'], function () {
    Route::view('/reset-password', 'layouts.auth')->name('password.reset');
    Route::view('/email/verify', 'layouts.auth')->name('verification.notice');
    Route::view('/{page}', 'layouts.auth')->where('page', '.*')->name('auth');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::view('/{page?}', 'layouts.admin')->where('page', '.*');
});

Route::view('/{page?}', 'layouts.main')->where('page', '.*')->middleware(['user', 'verified']);
