<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return new \App\Http\Resources\Auth\UserResource($request->user());
});

Route::group(['prefix' => 'posts', 'namespace' => 'Post'], function () {
    Route::get('/', 'IndexController');
    Route::post('/', 'StoreController');
    Route::patch('/{post}', 'UpdateController');
    Route::get('/{post}', 'ShowController');
    Route::delete('/{post}', 'DestroyController');
    Route::group(['prefix' => 'images', 'namespace' => 'Image'], function () {
        Route::post('/', 'StoreController');
    });
    Route::group(['prefix' => '/{post}/likes', 'namespace' => 'Like'], function(){
        Route::post('/', 'StoreController');
    });
});


Route::group(['prefix' => 'blog', 'namespace' => 'Blog'], function () {
    Route::get('/', 'IndexController');
});

Route::group(['prefix' => 'categories', 'namespace' => 'Category'], function () {
    Route::group(['prefix' => '{category}/posts', 'namespace' => 'Post'], function () {
        Route::get('/', 'IndexController');
    });
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::group(['prefix' => 'categories', 'namespace' => 'Category'], function () {
        Route::get('/', 'IndexController');
        Route::post('/', 'StoreController');
        Route::delete('/{category}', 'DestroyController');
        Route::patch('/{category}', 'UpdateController');
    });
    Route::group(['prefix' => 'tags', 'namespace' => 'Tag'], function () {
        Route::get('/', 'IndexController');
        Route::post('/', 'StoreController');
        Route::delete('/{tag}', 'DestroyController');
        Route::patch('/{tag}', 'UpdateController');
    });
    Route::group(['prefix' => 'users', 'namespace' => 'User'], function () {
        Route::get('/', 'IndexController');
        Route::get('/{user}', 'ShowController');
        Route::post('/', 'StoreController');
        Route::patch('/{user}', 'UpdateController');
        Route::delete('/{user}', 'DestroyController');
    });
    Route::group(['prefix' => 'roles', 'namespace' => 'Role'], function () {
        Route::get('/', 'IndexController');
    });
});


