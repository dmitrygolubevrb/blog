"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_Admin_Post_Update_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Admin/Post/Update.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Admin/Post/Update.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _lib_notification__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../lib/notification */ "./resources/js/lib/notification.js");
/* harmony import */ var _lib_rules__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../lib/rules */ "./resources/js/lib/rules.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _config_vueEditor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../config/vueEditor */ "./resources/js/config/vueEditor.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Create",
  data: function data() {
    return {
      editorSettings: _config_vueEditor__WEBPACK_IMPORTED_MODULE_2__["default"],
      isShowPreview: false
    };
  },
  computed: _objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_3__.mapGetters)(['categories', 'tags', 'tag', 'post', 'addTagInputIsVisible'])), {}, {
    mainImageUrl: {
      get: function get() {
        return this.$store.getters.mainImageUrl;
      },
      set: function set(url) {
        this.$store.commit('setMainImageUrl', url);
      }
    },
    mainImageIsZoom: {
      get: function get() {
        return this.$store.getters.mainImageIsZoom;
      },
      set: function set(bool) {
        this.$store.commit('setMainImageIsZoom', bool);
      }
    },
    notification: function notification() {
      return (0,_lib_notification__WEBPACK_IMPORTED_MODULE_0__["default"])(this.$notify);
    },
    rules: function rules() {
      return (0,_lib_rules__WEBPACK_IMPORTED_MODULE_1__["default"])(this.$store.getters.post);
    }
  }),
  mounted: function mounted() {
    this.$store.dispatch('getCategories', this.notification);
    this.$store.dispatch('getTags', this.notification);
    this.$store.dispatch('getAuthUserInfo');
    this.$store.dispatch('getPost', {
      id: this.$route.params.id,
      notification: this.notification
    });
  },
  methods: {
    updatePost: function updatePost(formName, data) {
      var _this = this;

      this.$refs[formName].validate(function (valid) {
        if (valid) _this.$store.dispatch('updatePost', data);
      });
    },
    handleRemove: function handleRemove(file, fileList) {
      this.$store.commit('setMainImage', null);
      this.$store.commit('setMainImageUrl', null);
    },
    handlePictureCardPreview: function handlePictureCardPreview(file) {
      this.$store.commit('setMainImageIsZoom', true);
    },
    handleAddMainImage: function handleAddMainImage(file) {
      this.$store.commit('setMainImageUrl', file.url);
      this.$store.commit('setMainImage', file.raw);
    },
    handleImageAdded: function handleImageAdded(file, Editor, cursorLocation, resetUploader) {
      var _this2 = this;

      var formData = new FormData();
      formData.append('image', file);
      axios.post('/api/posts/images', formData).then(function (res) {
        console.log(res);
        var url = res.data.url;
        Editor.insertEmbed(cursorLocation, 'image', url);
        resetUploader();
      })["catch"](function (error) {
        _this2.$notify({
          type: 'error',
          title: 'Ошибка загрузки',
          text: error.data.message
        });
      });
    },
    storeTag: function storeTag(data) {
      this.$store.dispatch('storeTag', data);
      this.$store.commit('setAddTagInputIsVisible', false);
      this.$store.commit('setTag', {
        id: null,
        title: null
      });
    },
    handleInputOut: function handleInputOut() {
      this.$store.commit('setAddTagInputIsVisible', false);
      this.$store.commit('setTag', {
        id: null,
        title: null
      });
    },
    handleShowInputTag: function handleShowInputTag() {
      var _this3 = this;

      this.$store.commit('setAddTagInputIsVisible', true);
      this.$nextTick(function () {
        _this3.$refs.addTagInput.$refs.input.focus();
      });
    }
  },
  beforeDestroy: function beforeDestroy() {
    this.$store.commit('setPost', {
      id: null,
      title: null,
      content_preview: null,
      content: null,
      category_id: null,
      tags_ids: [],
      main_image: null
    });
  }
});

/***/ }),

/***/ "./resources/js/config/vueEditor.js":
/*!******************************************!*\
  !*** ./resources/js/config/vueEditor.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vue2_editor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-editor */ "./node_modules/vue2-editor/dist/vue2-editor.esm.js");
/* harmony import */ var quill_image_drop_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! quill-image-drop-module */ "./node_modules/quill-image-drop-module/index.js");
/* harmony import */ var quill_resize_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! quill-resize-module */ "./node_modules/quill-resize-module/src/index.js");



vue2_editor__WEBPACK_IMPORTED_MODULE_0__.Quill.register("modules/imageResize", quill_resize_module__WEBPACK_IMPORTED_MODULE_2__["default"]);
vue2_editor__WEBPACK_IMPORTED_MODULE_0__.Quill.register("modules/imageDrop", quill_image_drop_module__WEBPACK_IMPORTED_MODULE_1__.ImageDrop);
var fonts = vue2_editor__WEBPACK_IMPORTED_MODULE_0__.Quill["import"]('attributors/class/font');
fonts.whitelist = ["sans-serif", "serif", "monospace"];
vue2_editor__WEBPACK_IMPORTED_MODULE_0__.Quill.register(fonts, true);
var editorSettings = {};
editorSettings.contentEditor = {
  modules: {
    imageDrop: true,
    imageResize: {
      theme: 'snow',
      modules: ['Resize', 'DisplaySize']
    },
    toolbar: [[{
      header: [false, 1, 2, 3, 4, 5, 6]
    }], ["bold", "italic", "underline", "strike"], // toggled buttons
    [{
      align: ""
    }, {
      align: "center"
    }, {
      align: "right"
    }, {
      align: "justify"
    }], ["blockquote", "code-block"], [{
      list: "ordered"
    }, {
      list: "bullet"
    }, {
      list: "check"
    }], [{
      indent: "-1"
    }, {
      indent: "+1"
    }], [{
      color: []
    }, {
      background: []
    }], ["link", "image", "video"], ["clean"], // This is what I have added
    [{
      'font': fonts.whitelist
    }]]
  }
};
editorSettings.previewContentEditorToolbar = [[{
  header: [false, 1, 2, 3, 4, 5, 6]
}], ["bold", "italic", "underline", "strike"], // toggled buttons
[{
  align: ""
}, {
  align: "center"
}, {
  align: "right"
}, {
  align: "justify"
}], ["blockquote"], [{
  indent: "-1"
}, {
  indent: "+1"
}], [{
  color: []
}, {
  background: []
}], [{
  'font': fonts.whitelist
}]];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (editorSettings);

/***/ }),

/***/ "./resources/js/lib/rules.js":
/*!***********************************!*\
  !*** ./resources/js/lib/rules.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(formData) {
  var validatePasswordConfirmed = function validatePasswordConfirmed(rule, value, callback) {
    if (value === '' || value === null) callback(new Error('Повторите пароль'));else if (value !== formData.password && value !== null) callback(new Error('Пароли должны совпадать'));
    callback();
  };

  var validatePassword = function validatePassword(rule, value, callback) {
    if (value === '' || value === null) callback(new Error('Введите пароль'));else if (value !== formData.password_confirmation && formData.password_confirmation !== undefined && formData.password_confirmation !== null) callback(new Error('Пароли должны совпадать'));
    callback();
  };

  return {
    password: [{
      validator: validatePassword,
      trigger: 'blur'
    }],
    password_confirmation: [{
      validator: validatePasswordConfirmed,
      trigger: 'blur'
    }],
    email: [{
      required: true,
      message: 'Введите email',
      trigger: 'blur'
    }, {
      type: 'email',
      message: 'Введите корректный email',
      trigger: 'blur'
    }],
    name: [{
      required: true,
      message: 'Введите имя',
      trigger: 'blur'
    }],
    title: [{
      required: true,
      message: 'Введите заголовок поста',
      trigger: 'blur'
    }],
    image: [{
      required: true,
      message: 'Необходимо выбрать изображение',
      trigger: 'blur'
    }],
    contentPreview: [{
      required: true,
      message: 'Необходимо указать превью',
      trigger: 'blur'
    }],
    content: [{
      required: true,
      message: 'Необходимо указать контент',
      trigger: 'blur'
    }],
    category: [{
      required: true,
      message: 'Необходимо выбрать категорию',
      trigger: 'blur'
    }],
    tagIds: [{
      required: true,
      message: 'Выберите хотя бы один тег',
      trigger: 'blur'
    }]
  };
}

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/quill-resize-module/src/assets/resize.css":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/quill-resize-module/src/assets/resize.css ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".ql-resize-style-left {\n    float: left;\n    margin: 0 1em 1em 0;\n}\n.ql-resize-style-center {\n    display: block;\n    margin: auto;\n}\n.ql-resize-style-right {\n    float: right;\n    margin: 0 0 1em 1em;\n}\n.ql-resize-style-full {\n    width: 100% !important;\n}\n\n.ql-embed-placeholder {\n    display: inline-flex;\n    flex-direction: column;\n    justify-content: center;\n    text-align: center;\n    width: 400px;\n    height: 225px;\n    background-color: #f0f0f0;\n    border: 1px solid #ccc;\n    cursor: default !important;\n    margin-bottom: 1em;\n    padding: 2em;\n    /* user-select: none; */\n}\n.ql-embed-placeholder:hover {\n    background-color: #e8e8e8;\n}\n.ql-embed-placeholder::before {\n    content: \"<\" attr(data-type) \">\";\n    font-size: 1.5em;\n    margin-bottom: .5em;\n    text-transform: uppercase;\n}\n.ql-embed-placeholder::after {\n    content: attr(data-src);\n    color: #666;\n}\n\n.ql-embed-placeholder.active {\n  border-color: #abc9e9;\n  background-color: #d2e5f9;\n}\n.ql-embed-placeholder.selected {\n  color: #fff;\n  border-color: #4185d2;\n  background-color: #5f9de2;\n}\n.ql-embed-placeholder.selected::after {\n  color: #fff;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/quill-resize-module/src/assets/pencil.svg":
/*!****************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/assets/pencil.svg ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/vendor/quill-resize-module/src/pencil.svg?53e4a255a334401797ce2bb834490196");

/***/ }),

/***/ "./node_modules/quill/assets/icons/float-center.svg":
/*!**********************************************************!*\
  !*** ./node_modules/quill/assets/icons/float-center.svg ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/vendor/quill/icons/float-center.svg?5793f183d1a93dc6f65ee9872c887088");

/***/ }),

/***/ "./node_modules/quill/assets/icons/float-full.svg":
/*!********************************************************!*\
  !*** ./node_modules/quill/assets/icons/float-full.svg ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/vendor/quill/icons/float-full.svg?928ea0b8deb9e17786f2bbcd77324de2");

/***/ }),

/***/ "./node_modules/quill/assets/icons/float-left.svg":
/*!********************************************************!*\
  !*** ./node_modules/quill/assets/icons/float-left.svg ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/vendor/quill/icons/float-left.svg?917812e3f874fe7614bdc5302f157012");

/***/ }),

/***/ "./node_modules/quill/assets/icons/float-right.svg":
/*!*********************************************************!*\
  !*** ./node_modules/quill/assets/icons/float-right.svg ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/vendor/quill/icons/float-right.svg?a595de253808219158737e11e9172a1a");

/***/ }),

/***/ "./node_modules/quill-image-drop-module/index.js":
/*!*******************************************************!*\
  !*** ./node_modules/quill-image-drop-module/index.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ImageDrop": () => (/* binding */ ImageDrop)
/* harmony export */ });
/**
 * Custom module for quilljs to allow user to drag images from their file system into the editor
 * and paste images from clipboard (Works on Chrome, Firefox, Edge, not on Safari)
 * @see https://quilljs.com/blog/building-a-custom-module/
 */
class ImageDrop {

	/**
	 * Instantiate the module given a quill instance and any options
	 * @param {Quill} quill
	 * @param {Object} options
	 */
	constructor(quill, options = {}) {
		// save the quill reference
		this.quill = quill;
		// bind handlers to this instance
		this.handleDrop = this.handleDrop.bind(this);
		this.handlePaste = this.handlePaste.bind(this);
		// listen for drop and paste events
		this.quill.root.addEventListener('drop', this.handleDrop, false);
		this.quill.root.addEventListener('paste', this.handlePaste, false);
	}

	/**
	 * Handler for drop event to read dropped files from evt.dataTransfer
	 * @param {Event} evt
	 */
	handleDrop(evt) {
		evt.preventDefault();
		if (evt.dataTransfer && evt.dataTransfer.files && evt.dataTransfer.files.length) {
			if (document.caretRangeFromPoint) {
				const selection = document.getSelection();
				const range = document.caretRangeFromPoint(evt.clientX, evt.clientY);
				if (selection && range) {
					selection.setBaseAndExtent(range.startContainer, range.startOffset, range.startContainer, range.startOffset);
				}
			}
			this.readFiles(evt.dataTransfer.files, this.insert.bind(this));
		}
	}

	/**
	 * Handler for paste event to read pasted files from evt.clipboardData
	 * @param {Event} evt
	 */
	handlePaste(evt) {
		if (evt.clipboardData && evt.clipboardData.items && evt.clipboardData.items.length) {
			this.readFiles(evt.clipboardData.items, dataUrl => {
				const selection = this.quill.getSelection();
				if (selection) {
					// we must be in a browser that supports pasting (like Firefox)
					// so it has already been placed into the editor
				}
				else {
					// otherwise we wait until after the paste when this.quill.getSelection()
					// will return a valid index
					setTimeout(() => this.insert(dataUrl), 0);
				}
			});
		}
	}

	/**
	 * Insert the image into the document at the current cursor position
	 * @param {String} dataUrl  The base64-encoded image URI
	 */
	insert(dataUrl) {
		const index = (this.quill.getSelection() || {}).index || this.quill.getLength();
		this.quill.insertEmbed(index, 'image', dataUrl, 'user');
	}

	/**
	 * Extract image URIs a list of files from evt.dataTransfer or evt.clipboardData
	 * @param {File[]} files  One or more File objects
	 * @param {Function} callback  A function to send each data URI to
	 */
	readFiles(files, callback) {
		// check each file for an image
		[].forEach.call(files, file => {
			if (!file.type.match(/^image\/(gif|jpe?g|a?png|svg|webp|bmp|vnd\.microsoft\.icon)/i)) {
				// file is not an image
				// Note that some file formats such as psd start with image/* but are not readable
				return;
			}
			// set up file reader
			const reader = new FileReader();
			reader.onload = (evt) => {
				callback(evt.target.result);
			};
			// read the clipboard item or file
			const blob = file.getAsFile ? file.getAsFile() : file;
			if (blob instanceof Blob) {
				reader.readAsDataURL(blob);
			}
		});
	}

}


/***/ }),

/***/ "./node_modules/quill-resize-module/src/DefaultOptions.js":
/*!****************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/DefaultOptions.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  modules: ['DisplaySize', 'Toolbar', 'Resize', 'Keyboard'],
  keyboardSelect: true,
  selectedClass: 'selected',
  activeClass: 'active',

  parchment: {
    image: {
      attribute: ['width'],
      limit: {
        minWidth: 100
      }
    },
    'embed-placeholder': {
      attribute: ['width', 'height'],
      limit: {
        minWidth: 200,
        ratio: 0.5625
      }
    },
    video: {
      attribute: ['width', 'height'],
      limit: {
        minWidth: 200,
        ratio: 0.5625
      }
    }
  },

  styles: {
    overlay: {
      position: 'absolute',
      boxSizing: 'border-box',
      border: '1px dashed #444'
    },
    handle: {
      position: 'absolute',
      height: '12px',
      width: '12px',
      backgroundColor: 'white',
      border: '1px solid #777',
      boxSizing: 'border-box',
      opacity: '0.80'
    },
    display: {
      position: 'absolute',
      padding: '4px 8px',
      textAlign: 'center',
      backgroundColor: 'white',
      color: '#333',
      border: '1px solid #777',
      boxSizing: 'border-box',
      opacity: '0.80',
      cursor: 'default',
      lineHeight: '1'
    },
    toolbar: {
      position: 'absolute',
      top: '-12px',
      right: '0',
      left: '0',
      height: '0',
      minWidth: '120px',
      textAlign: 'center',
      color: '#333',
      boxSizing: 'border-box',
      cursor: 'default'
    },
    toolbarButton: {
      display: 'inline-block',
      width: '24px',
      height: '24px',
      background: 'white',
      border: '1px solid #999',
      verticalAlign: 'middle'
    },
    toolbarButtonSvg: {}
  }
});


/***/ }),

/***/ "./node_modules/quill-resize-module/src/QuillResize.js":
/*!*************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/QuillResize.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ QuillResize)
/* harmony export */ });
/* harmony import */ var _DefaultOptions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DefaultOptions */ "./node_modules/quill-resize-module/src/DefaultOptions.js");
/* harmony import */ var _modules_DisplaySize__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/DisplaySize */ "./node_modules/quill-resize-module/src/modules/DisplaySize.js");
/* harmony import */ var _modules_Toolbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/Toolbar */ "./node_modules/quill-resize-module/src/modules/Toolbar.js");
/* harmony import */ var _modules_Resize__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/Resize */ "./node_modules/quill-resize-module/src/modules/Resize.js");
/* harmony import */ var _modules_Keyboard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/Keyboard */ "./node_modules/quill-resize-module/src/modules/Keyboard.js");
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! quill */ "./node_modules/quill/dist/quill.js");
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(quill__WEBPACK_IMPORTED_MODULE_5__);







const Quill = window.Quill || (quill__WEBPACK_IMPORTED_MODULE_5___default())
const Parchment = Quill.import('parchment')

const knownModules = { DisplaySize: _modules_DisplaySize__WEBPACK_IMPORTED_MODULE_1__["default"], Toolbar: _modules_Toolbar__WEBPACK_IMPORTED_MODULE_2__["default"], Resize: _modules_Resize__WEBPACK_IMPORTED_MODULE_3__["default"], Keyboard: _modules_Keyboard__WEBPACK_IMPORTED_MODULE_4__["default"] }

/**
 * Custom module for quilljs to allow user to resize elements
 * (Works on Chrome, Edge, Safari and replaces Firefox's native resize behavior)
 * @see https://quilljs.com/blog/building-a-custom-module/
 */
class QuillResize {
  constructor (quill, options = {}) {
    quill.resizer = this
    // save the quill reference and options
    this.quill = quill

    // Apply the options to our defaults, and stash them for later
    // defaultsDeep doesn't do arrays as you'd expect, so we'll need to apply the classes array from options separately
    let moduleClasses = false
    if (options.modules) {
      moduleClasses = options.modules.slice()
    }

    // Apply options to default options
    this.options = Object.assign({}, _DefaultOptions__WEBPACK_IMPORTED_MODULE_0__["default"], options)
    this.options.styles = Object.assign({}, _DefaultOptions__WEBPACK_IMPORTED_MODULE_0__["default"].styles, options.styles)

    // (see above about moduleClasses)
    if (moduleClasses !== false) {
      this.options.modules = moduleClasses
    }

    // disable native image resizing on firefox
    document.execCommand('enableObjectResizing', false, 'false')

    // respond to clicks inside the editor
    this.quill.root.addEventListener(
      'mousedown',
      this.handleClick.bind(this),
      false
    )

    this.quill.on('text-change', this.handleChange.bind(this))

    this.quill.emitter.on('resize-edit', this.handleEdit.bind(this))

    this.quill.root.parentNode.style.position =
      this.quill.root.parentNode.style.position || 'relative'

    // add class to selected parchment
    this.selectedBlots = []
    if (this.options.selectedClass) {
      this.quill.on('selection-change', this.addBlotsSelectedClass.bind(this))
    }

    // setup modules
    this.moduleClasses = this.options.modules

    this.modules = []

    // inject keyboard event
    if (this.options.keyboardSelect) {
      _modules_Keyboard__WEBPACK_IMPORTED_MODULE_4__["default"].injectInit(this.quill)
    }
  }

  initializeModules () {
    this.removeModules()

    this.modules = this.moduleClasses.map(
      ModuleClass => new (knownModules[ModuleClass] || ModuleClass)(this)
    )

    this.modules.forEach(module => {
      module.onCreate()
    })

    this.onUpdate()
  }

  onUpdate (fromModule) {
    this.updateFromModule = fromModule
    this.repositionElements()
    this.modules.forEach(module => {
      module.onUpdate()
    })
  }

  removeModules () {
    this.modules.forEach(module => {
      module.onDestroy()
    })

    this.modules = []
  }

  handleEdit () {
    if (!this.blot) return
    const index = this.blot.offset(this.quill.scroll)
    this.hide()
    this.quill.focus()
    this.quill.setSelection(index, 1)
  }

  handleClick (evt) {
    let show = false
    let blot
    const target = evt.target

    if (target && target.tagName) {
      blot = this.quill.constructor.find(target)
      if (blot) {
        show = this.judgeShow(blot, target)
      }
    }
    if (show) {
      evt.preventDefault()
      // evt.stopPropagation()
      return
    }
    if (this.activeEle) {
      // clicked on a non image
      this.hide()
    }
  }

  judgeShow (blot, target) {
    let res = false
    if (!blot) return res

    if (!target && blot.domNode) target = blot.domNode
    const options = this.options.parchment[blot.statics.blotName]
    if (!options) return res
    if (this.activeEle === target) return true

    const limit = options.limit || {}
    if (
      !limit.minWidth ||
      (limit.minWidth && target.offsetWidth >= limit.minWidth)
    ) {
      res = true

      if (this.activeEle) {
        // we were just focused on another image
        this.hide()
      }
      // keep track of this img element
      this.activeEle = target
      this.blot = blot
      // clicked on an image inside the editor
      this.show(target)
    }

    return res
  }

  handleChange (delta, oldDelta, source) {
    if (this.updateFromModule) {
      this.updateFromModule = false
      return
    }

    if (source !== 'user' || !this.overlay || !this.activeEle) return
    this.onUpdate()
  }

  show () {
    this.showOverlay()
    this.initializeModules()
    if (this.options.activeClass) this.activeEle.classList.add(this.options.activeClass)
  }

  showOverlay () {
    if (this.overlay) {
      this.hideOverlay()
    }

    this.quill.setSelection(null)

    // prevent spurious text selection
    this.setUserSelect('none')

    // Create and add the overlay
    this.overlay = document.createElement('div')
    // this.overlay.setAttribute('title', "Double-click to select image");
    Object.assign(this.overlay.style, this.options.styles.overlay)
    this.overlay.addEventListener('dblclick', this.handleEdit.bind(this), false)

    this.quill.root.parentNode.appendChild(this.overlay)

    this.hideProxy = evt => {
      if (!this.activeEle) return
      this.hide()
    }
    // listen for the image being deleted or moved
    this.quill.root.addEventListener('input', this.hideProxy, true)

    this.updateOverlayPositionProxy = this.updateOverlayPosition.bind(this)
    this.quill.root.addEventListener('scroll', this.updateOverlayPositionProxy)

    this.repositionElements()
  }

  hideOverlay () {
    if (!this.overlay) {
      return
    }

    // Remove the overlay
    this.quill.root.parentNode.removeChild(this.overlay)
    this.overlay = undefined

    // stop listening for image deletion or movement
    document.removeEventListener('keydown', this.keyboardProxy, true)
    this.quill.root.removeEventListener('input', this.hideProxy, true)
    this.quill.root.removeEventListener('scroll', this.updateOverlayPositionProxy)

    // reset user-select
    this.setUserSelect('')
  }

  repositionElements () {
    if (!this.overlay || !this.activeEle) {
      return
    }

    // position the overlay over the image
    const parent = this.quill.root.parentNode
    const eleRect = this.activeEle.getBoundingClientRect()
    const containerRect = parent.getBoundingClientRect()

    Object.assign(this.overlay.style, {
      left: `${eleRect.left - containerRect.left - 1 + parent.scrollLeft}px`,
      top: `${eleRect.top - containerRect.top + this.quill.root.scrollTop}px`,
      width: `${eleRect.width}px`,
      height: `${eleRect.height}px`,
      marginTop: -1 * this.quill.root.scrollTop + 'px'
    })
  }

  updateOverlayPosition () {
    this.overlay.style.marginTop = -1 * this.quill.root.scrollTop + 'px'
  }

  addBlotsSelectedClass (range, oldRange) {
    if (!range) {
      this.removeBlotsSelectedClass()
      this.selectedBlots = []
      return
    }
    const leaves = this.quill.scroll.descendants(Parchment.Leaf, range.index, range.length)
    const blots = leaves.filter(blot => {
      const canBeHandle = !!this.options.parchment[blot.statics.blotName]
      if (canBeHandle) blot.domNode.classList.add(this.options.selectedClass)
      return canBeHandle
    })
    this.removeBlotsSelectedClass(blots)
    this.selectedBlots = blots
  }

  removeBlotsSelectedClass (ignoreBlots = []) {
    if (!Array.isArray(ignoreBlots)) ignoreBlots = [ignoreBlots]

    this.selectedBlots.forEach(blot => {
      if (ignoreBlots.indexOf(blot) === -1) {
        blot.domNode.classList.remove(this.options.selectedClass)
      }
    })
  }

  hide () {
    this.hideOverlay()
    this.removeModules()
    if (this.activeEle && this.options.activeClass) this.activeEle.classList.remove(this.options.activeClass)
    this.activeEle = undefined
    this.blot = undefined
  }

  setUserSelect (value) {
    [
      'userSelect',
      'mozUserSelect',
      'webkitUserSelect',
      'msUserSelect'
    ].forEach(prop => {
      // set on contenteditable element and <html>
      this.quill.root.style[prop] = value
      document.documentElement.style[prop] = value
    })
  }
}

if (window.Quill) {
  window.Quill.register('modules/resize', QuillResize)
}


/***/ }),

/***/ "./node_modules/quill-resize-module/src/formats/image.js":
/*!***************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/formats/image.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ATTRIBUTES": () => (/* binding */ ATTRIBUTES),
/* harmony export */   "Image": () => (/* binding */ Image)
/* harmony export */ });
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! quill */ "./node_modules/quill/dist/quill.js");
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(quill__WEBPACK_IMPORTED_MODULE_0__);

const Quill = window.Quill || (quill__WEBPACK_IMPORTED_MODULE_0___default())

// BEGIN allow image alignment styles
const ATTRIBUTES = ['alt', 'height', 'width', 'style', 'data-size']

var BaseImageFormat = Quill.import('formats/image')
class Image extends BaseImageFormat {
  static formats (domNode) {
    if (domNode.__handling && domNode.__formats) {
      return domNode.__formats
    }

    return ATTRIBUTES.reduce(function (formats, attribute) {
      if (domNode.hasAttribute(attribute)) {
        formats[attribute] = domNode.getAttribute(attribute)
      }
      return formats
    }, {})
  }

  format (name, value) {
    if (ATTRIBUTES.indexOf(name) > -1) {
      if (value) {
        this.domNode.setAttribute(name, value)
      } else {
        this.domNode.removeAttribute(name)
      }
    } else {
      super.format(name, value)
    }
  }

  handling (handling) {
    this.domNode.__formats = this.constructor.formats(this.domNode)
    this.domNode.__handling = handling
  }
}




/***/ }),

/***/ "./node_modules/quill-resize-module/src/formats/placeholder.js":
/*!*********************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/formats/placeholder.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ClassNamePlaceholder": () => (/* binding */ ClassNamePlaceholder),
/* harmony export */   "EmbedPlaceholder": () => (/* binding */ EmbedPlaceholder),
/* harmony export */   "TagPlaceholder": () => (/* binding */ TagPlaceholder),
/* harmony export */   "convertPlaceholderHTML": () => (/* binding */ convertPlaceholderHTML),
/* harmony export */   "default": () => (/* binding */ register)
/* harmony export */ });
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! quill */ "./node_modules/quill/dist/quill.js");
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(quill__WEBPACK_IMPORTED_MODULE_0__);

const Quill = window.Quill || (quill__WEBPACK_IMPORTED_MODULE_0___default())

const Container = Quill.import('blots/container')
const Scroll = Quill.import('blots/scroll')

const ATTRIBUTES = [
  'data-embed-source',
  'data-type',
  'data-src',
  'data-size',
  'style'
]
const Parchment = Quill.import('parchment')
const Embed = Quill.import('blots/block/embed')

class EmbedPlaceholder extends Embed {
  static create (value) {
    const node = super.create()
    if (typeof value === 'string') {
      node.setAttribute(ATTRIBUTES[0], value)
    } else {
      for (const key in value) {
        if (!Object.prototype.hasOwnProperty.call(value, key)) continue
        node.setAttribute(key, value[key])
      }
    }

    node.setAttribute('contenteditable', false)
    node.setAttribute('unselectable', 'on')
    // node.setAttribute('title', node.textContent);
    return node
  }

  static formats (domNode) {
    if (domNode.__handling && domNode.__formats) {
      return domNode.__formats
    }

    const attrList = ATTRIBUTES.slice(3)
    return attrList.reduce(function (formats, attribute) {
      if (domNode.hasAttribute(attribute)) {
        formats[attribute] = domNode.getAttribute(attribute)
      }
      return formats
    }, {})
  }

  static value (domNode) {
    const attrs = ATTRIBUTES.slice(0, 3)

    const result = {}

    attrs.forEach(attr => {
      let res = ''
      if (domNode.hasAttribute(attr)) {
        res = domNode.getAttribute(attr)
      } else {
        switch (attr) {
          case ATTRIBUTES[0]:
            res = encodeURIComponent(domNode.outerHTML)
            break
          case ATTRIBUTES[1]:
            res = domNode.tagName
            break
          case ATTRIBUTES[2]:
            res = domNode.getAttribute('src')
            break
          case 'style':
            res = domNode.style.cssText
            break
          default:
            res = domNode[attr] || ''
        }
      }

      if (res) result[attr] = res
    })

    return result
  }

  format (name, value) {
    if (name === 'style') {
      this.domNode.style.cssText = value
      return
    }
    if (ATTRIBUTES.indexOf(name) === -1) {
      super.format(name, value)
      return
    }

    if (value) {
      this.domNode.setAttribute(name, value)
    } else {
      this.domNode.removeAttribute(name)
    }
  }

  handling (handling) {
    this.domNode.__formats = this.constructor.formats(this.domNode)
    this.domNode.__handling = handling
  }
}
EmbedPlaceholder.blotName = 'embed-placeholder'
EmbedPlaceholder.tagName = 'span'
EmbedPlaceholder.scope = Parchment.Scope.INLINE_BLOT

Container.allowedChildren.push(EmbedPlaceholder)
Scroll.allowedChildren.push(EmbedPlaceholder)

class TagPlaceholder extends EmbedPlaceholder {}
TagPlaceholder.tagName = ['video', 'iframe']

class ClassNamePlaceholder extends EmbedPlaceholder {}
ClassNamePlaceholder.className = 'ql-embed-placeholder'

const tagReg = /<([\w-]+)((?:\s+[\w-:.]*(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*>([^<]*?)<\/\1>/g
const attrReg = /([\w-:.]+)(?:\s*=\s*(?:"((?:\\.|[^"])*)"|'((?:\\.|[^'])*)'))?/g
function convertPlaceholderHTML (html = '') {
  if (!html) return ''

  const matchReg = new RegExp(
    `class\\s*=\\s*(?:"[^"]*\\b(${ClassNamePlaceholder.className})\\b[^"]*"|'[^']*\\b(${ClassNamePlaceholder.className})\\b[^']*')`
  )
  return html.replace(tagReg, (m, tag, attrs = '') => {
    if (
      !tag ||
      tag.toLowerCase() !== EmbedPlaceholder.tagName ||
      !matchReg.test(attrs)
    ) {
      return m
    }

    const attributes = getAttributes(attrs)
    const source = decodeURIComponent(attributes[ATTRIBUTES[0]])
    // if (!attributes.style) return source

    return replaceHTMLAttr(source, {
      style: attributes.style,
      'data-size': attributes['data-size']
    })
  })
}

function getAttributes (str) {
  const attributes = {}
  str.replace(attrReg, (m, name, attr1, attr2) => {
    const attr = (attr1 || attr2 || '').trim()
    attributes[name] = attr
  })

  return attributes
}

const sourceTagReg = /<([\w-]+)((?:\s+[\w-:.]*(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*>/
function replaceHTMLAttr (html = '', attrs = {}) {
  return html.replace(sourceTagReg, (m, tag, attr = '') => {
    const attributes = getAttributes(attr)
    Object.assign(attributes, attrs)

    const attrsStr = Object.keys(attributes).reduce((str, key) => {
      const val = attributes[key]
      if (val == null) return str
      str += val === '' ? ` ${key}` : ` ${key}="${val}"`
      return str
    }, '')

    return `<${tag}${attrsStr}>`
  })
}

function register (formats = [TagPlaceholder]) {
  if (!Array.isArray(formats)) formats = [formats]
  formats.push(ClassNamePlaceholder)
  formats.forEach(fmt => {
    Quill.register(fmt, true)
    fmt.tagName = EmbedPlaceholder.tagName
    fmt.className = ClassNamePlaceholder.className
  })
}




/***/ }),

/***/ "./node_modules/quill-resize-module/src/index.js":
/*!*******************************************************!*\
  !*** ./node_modules/quill-resize-module/src/index.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ClassNamePlaceholder": () => (/* reexport safe */ _formats_placeholder__WEBPACK_IMPORTED_MODULE_4__.ClassNamePlaceholder),
/* harmony export */   "EmbedPlaceholder": () => (/* reexport safe */ _formats_placeholder__WEBPACK_IMPORTED_MODULE_4__.EmbedPlaceholder),
/* harmony export */   "Image": () => (/* reexport safe */ _formats_image__WEBPACK_IMPORTED_MODULE_3__.Image),
/* harmony export */   "PlaceholderRegister": () => (/* reexport safe */ _formats_placeholder__WEBPACK_IMPORTED_MODULE_4__["default"]),
/* harmony export */   "Resize": () => (/* reexport safe */ _QuillResize__WEBPACK_IMPORTED_MODULE_2__["default"]),
/* harmony export */   "TagPlaceholder": () => (/* reexport safe */ _formats_placeholder__WEBPACK_IMPORTED_MODULE_4__.TagPlaceholder),
/* harmony export */   "convertPlaceholderHTML": () => (/* reexport safe */ _formats_placeholder__WEBPACK_IMPORTED_MODULE_4__.convertPlaceholderHTML),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! quill */ "./node_modules/quill/dist/quill.js");
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(quill__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _assets_resize_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./assets/resize.css */ "./node_modules/quill-resize-module/src/assets/resize.css");
/* harmony import */ var _QuillResize__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./QuillResize */ "./node_modules/quill-resize-module/src/QuillResize.js");
/* harmony import */ var _formats_image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./formats/image */ "./node_modules/quill-resize-module/src/formats/image.js");
/* harmony import */ var _formats_placeholder__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./formats/placeholder */ "./node_modules/quill-resize-module/src/formats/placeholder.js");







const Quill = window.Quill || (quill__WEBPACK_IMPORTED_MODULE_0___default())
Quill.register(_formats_image__WEBPACK_IMPORTED_MODULE_3__.Image, true)



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_QuillResize__WEBPACK_IMPORTED_MODULE_2__["default"]);


// Polyfill for IE and Element.closest
if (!Element.prototype.matches) {
  Element.prototype.matches =
    Element.prototype.msMatchesSelector ||
    Element.prototype.webkitMatchesSelector
}

if (!Element.prototype.closest) {
  Element.prototype.closest = function (s) {
    var el = this
    if (!document.documentElement.contains(el)) return null
    do {
      if (el.matches(s)) return el
      el = el.parentElement || el.parentNode
    } while (el !== null && el.nodeType === 1)
    return null
  }
}


/***/ }),

/***/ "./node_modules/quill-resize-module/src/modules/BaseModule.js":
/*!********************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/modules/BaseModule.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ BaseModule)
/* harmony export */ });
class BaseModule {
  constructor (resizer) {
    this.resizer = resizer
    this.quill = resizer.quill
    this.overlay = resizer.overlay
    this.activeEle = resizer.activeEle
    this.blot = resizer.blot
    this.options = resizer.options
    this.requestUpdate = () => {
      resizer.onUpdate(true)
    }
  }
  /*
    requestUpdate (passed in by the library during construction, above) can be used to let the library know that
    you've changed something about the image that would require re-calculating the overlay (and all of its child
    elements)

    For example, if you add a margin to the element, you'll want to call this or else all the controls will be
    misaligned on-screen.
  */

  /*
    onCreate will be called when the element is clicked on

    If the module has any user controls, it should create any containers that it'll need here.
    The overlay has absolute positioning, and will be automatically repositioned and resized as needed, so you can
    use your own absolute positioning and the 'top', 'right', etc. styles to be positioned relative to the element
    on-screen.
  */
  onCreate () {}

  /*
    onDestroy will be called when the element is de-selected, or when this module otherwise needs to tidy up.

    If you created any DOM elements in onCreate, please remove them from the DOM and destroy them here.
  */
  onDestroy () {}

  /*
    onUpdate will be called any time that the element is changed (e.g. resized, aligned, etc.)

    This frequently happens during resize dragging, so keep computations light while here to ensure a smooth
    user experience.
  */
  onUpdate () {}
}


/***/ }),

/***/ "./node_modules/quill-resize-module/src/modules/DisplaySize.js":
/*!*********************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/modules/DisplaySize.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ DisplaySize)
/* harmony export */ });
/* harmony import */ var _BaseModule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BaseModule */ "./node_modules/quill-resize-module/src/modules/BaseModule.js");


class DisplaySize extends _BaseModule__WEBPACK_IMPORTED_MODULE_0__["default"] {
  onCreate () {
    // Create the container to hold the size display
    this.display = document.createElement('div')

    // Apply styles
    Object.assign(this.display.style, this.options.styles.display)

    // Attach it
    this.overlay.appendChild(this.display)
  }

  onUpdate () {
    if (!this.display || !this.activeEle) {
      return
    }

    const size = this.getCurrentSize()
    this.display.innerHTML = size.join(' &times; ')
    if (size[0] > 120 && size[1] > 30) {
      // position on top of image
      Object.assign(this.display.style, {
        right: '4px',
        bottom: '4px',
        left: 'auto'
      })
    } else if (this.activeEle.style.float === 'right') {
      // position off bottom left
      const displayRect = this.display.getBoundingClientRect()
      Object.assign(this.display.style, {
        right: 'auto',
        bottom: `-${displayRect.height + 4}px`,
        left: `-${displayRect.width + 4}px`
      })
    } else {
      // position off bottom right
      const displayRect = this.display.getBoundingClientRect()
      Object.assign(this.display.style, {
        right: `-${displayRect.width + 4}px`,
        bottom: `-${displayRect.height + 4}px`,
        left: 'auto'
      })
    }
  }

  getCurrentSize () {
    return [this.activeEle.offsetWidth, this.activeEle.offsetHeight]
  }
}


/***/ }),

/***/ "./node_modules/quill-resize-module/src/modules/Keyboard.js":
/*!******************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/modules/Keyboard.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Keyboard)
/* harmony export */ });
/* harmony import */ var _BaseModule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BaseModule */ "./node_modules/quill-resize-module/src/modules/BaseModule.js");


class Keyboard extends _BaseModule__WEBPACK_IMPORTED_MODULE_0__["default"] {
  static injectInit (quill) {
    // left/right
    const bindings = quill.keyboard.bindings
    bindings[this.keys.LEFT].unshift(
      this.makeArrowHandler(this.keys.LEFT, false)
    )
    bindings[this.keys.RIGHT].unshift(
      this.makeArrowHandler(this.keys.RIGHT, false)
    )

    // // up
    // const upBinding = this.makeArrowHandler(this.keys.UP, false)
    // if (bindings[this.keys.UP]) {
    //   bindings[this.keys.UP].unshift(upBinding)
    // } else {
    //   quill.keyboard.addBinding(upBinding)
    // }
    // // down
    // const downBinding = this.makeArrowHandler(this.keys.DOWN, false)
    // if (bindings[this.keys.DOWN]) {
    //   bindings[this.keys.DOWN].unshift(downBinding)
    // } else {
    //   quill.keyboard.addBinding(downBinding)
    // }
  }

  static makeArrowHandler (key, shiftKey) {
    const where = key === Keyboard.keys.LEFT ? 'prefix' : 'suffix'
    return {
      key,
      shiftKey,
      altKey: null,
      [where]: /^$/,
      handler: function (range) {
        if (!this.quill.resizer) return true

        let index = range.index

        // check end of line
        const [line] = this.quill.getLine(range.index)
        const lineIndex = this.quill.getIndex(line)
        if (key === Keyboard.keys.RIGHT && lineIndex + line.length() - 1 === index) return true

        // get leaf/offset
        if (key === Keyboard.keys.RIGHT) {
          index += (range.length + 1)
        }
        let [leaf] = this.quill.getLeaf(index)
        const offset = leaf.offset(leaf.parent)

        // check start of line
        if (key === Keyboard.keys.LEFT && (index === 0 || index === lineIndex)) return true

        // get previous leaf
        if (key === Keyboard.keys.LEFT) {
          if (offset === 0) {
            index -= 1
            leaf = this.quill.getLeaf(index)[0]
          }
        }

        return !this.quill.resizer.judgeShow(leaf)
      }
    }
  }

  onCreate (e) {
    this.keyboardProxy = evt => this.keyboardHandle(evt)
    document.addEventListener('keydown', this.keyboardProxy, true)
  }

  onDestroy () {
    document.removeEventListener('keydown', this.keyboardProxy, true)
  }

  keyboardHandle (evt) {
    if (evt.defaultPrevented) return
    if (evt.shiftKey || evt.ctrlKey || evt.altKey) {
      return
    }
    if (!this.activeEle || evt.fromResize || evt.ctrlKey) return

    const code = evt.keyCode
    let index = this.blot.offset(this.quill.scroll)
    let nextBlot
    let handled = false

    // delete
    if (code === Keyboard.keys.BACKSPACE || code === Keyboard.keys.DELETE) {
      this.blot.deleteAt(0)
      this.blot.parent.optimize()
      handled = true

    // direction key
    } else if (code >= Keyboard.keys.LEFT && code <= Keyboard.keys.DOWN) {
      if (code === Keyboard.keys.RIGHT) {
        index++
      } else if (code === Keyboard.keys.UP) {
        index = this.getOtherLineIndex(-1)
        nextBlot = this.quill.getLeaf(index)[0]
      } else if (code === Keyboard.keys.DOWN) {
        index = this.getOtherLineIndex(1)
        nextBlot = this.quill.getLeaf(index)[0]
      }
      handled = true
    }

    if (handled) {
      evt.stopPropagation()
      evt.preventDefault()
    }

    if (nextBlot && this.resizer.judgeShow(nextBlot, nextBlot.domNode)) return

    this.quill.setSelection(index)
    this.resizer.hide()
  }

  getOtherLineIndex (dir) {
    let index = this.blot.offset(this.quill.scroll)
    const [line] = this.quill.getLine(index)
    const lineIndex = this.blot.offset(line) + 1

    const otherLine = dir > 0 ? line.next : line.prev

    if (otherLine) {
      let len = otherLine.length()
      if (otherLine.statics.blotName === 'block') len--
      index = otherLine.offset(this.quill.scroll) + Math.min(len, lineIndex)
    }

    return index
  }

  // 转发event到quill
  dispatchEvent (evt) {
    const event = new evt.constructor(evt)
    event.fromResize = true
    this.quill.root.dispatchEvent(event)
  }
}

Keyboard.keys = {
  BACKSPACE: 8,
  TAB: 9,
  ENTER: 13,
  ESCAPE: 27,
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,
  DELETE: 46
}


/***/ }),

/***/ "./node_modules/quill-resize-module/src/modules/Resize.js":
/*!****************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/modules/Resize.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Resize)
/* harmony export */ });
/* harmony import */ var _BaseModule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BaseModule */ "./node_modules/quill-resize-module/src/modules/BaseModule.js");


class Resize extends _BaseModule__WEBPACK_IMPORTED_MODULE_0__["default"] {
  onCreate () {
    // track resize handles
    this.boxes = []

    // add 4 resize handles
    this.addBox('nwse-resize') // top left
    this.addBox('nesw-resize') // top right
    this.addBox('nwse-resize') // bottom right
    this.addBox('nesw-resize') // bottom left

    this.positionBoxes()
  }

  onDestroy () {
    // reset drag handle cursors
    this.setCursor('')
  }

  positionBoxes () {
    const handleXOffset = `${-parseFloat(this.options.styles.handle.width) /
      2}px`
    const handleYOffset = `${-parseFloat(this.options.styles.handle.height) /
      2}px`

    // set the top and left for each drag handle
    ;[
      { left: handleXOffset, top: handleYOffset }, // top left
      { right: handleXOffset, top: handleYOffset }, // top right
      { right: handleXOffset, bottom: handleYOffset }, // bottom right
      { left: handleXOffset, bottom: handleYOffset } // bottom left
    ].forEach((pos, idx) => {
      Object.assign(this.boxes[idx].style, pos)
    })
  }

  addBox (cursor) {
    // create div element for resize handle
    const box = document.createElement('div')

    // Star with the specified styles
    Object.assign(box.style, this.options.styles.handle)
    box.style.cursor = cursor

    // Set the width/height to use 'px'
    box.style.width = `${this.options.styles.handle.width}px`
    box.style.height = `${this.options.styles.handle.height}px`

    // listen for mousedown on each box
    box.addEventListener('mousedown', this.handleMousedown.bind(this), false)
    // add drag handle to document
    this.overlay.appendChild(box)
    // keep track of drag handle
    this.boxes.push(box)
  }

  handleMousedown (evt) {
    this.blot.handling && this.blot.handling(true)
    // note which box
    this.dragBox = evt.target
    // note starting mousedown position
    this.dragStartX = evt.clientX
    this.dragStartY = evt.clientY
    // store the width before the drag
    this.preDragSize = {
      width: this.activeEle.offsetWidth,
      height: this.activeEle.offsetHeight
    }
    // store the natural size
    this.naturalSize = this.getNaturalSize()
    // set the proper cursor everywhere
    this.setCursor(this.dragBox.style.cursor)

    this.handleDragProxy = evt => this.handleDrag(evt)
    this.handleMouseupProxy = evt => this.handleMouseup(evt)
    // listen for movement and mouseup
    document.addEventListener('mousemove', this.handleDragProxy, false)
    document.addEventListener('mouseup', this.handleMouseupProxy, false)
  }

  handleMouseup () {
    // reset cursor everywhere
    this.setCursor('')
    this.blot.handling && this.blot.handling(false)
    // stop listening for movement and mouseup
    document.removeEventListener('mousemove', this.handleDragProxy)
    document.removeEventListener('mouseup', this.handleMouseupProxy)
  }

  handleDrag (evt) {
    if (!this.activeEle || !this.blot) {
      // activeEle not set yet
      return
    }
    // update size
    const deltaX = evt.clientX - this.dragStartX
    const deltaY = evt.clientY - this.dragStartY

    const options = this.options.parchment[this.blot.statics.blotName]
    const size = {}
    let direction = 1

    ;(options.attribute || ['width']).forEach(key => {
      size[key] = this.preDragSize[key]
    })

    // left-side
    if (this.dragBox === this.boxes[0] || this.dragBox === this.boxes[3]) {
      direction = -1
    }

    if (size.width) {
      size.width = Math.round(this.preDragSize.width + deltaX * direction)
    }
    if (size.height) {
      size.height = Math.round(this.preDragSize.height + deltaY * direction)
    }

    Object.assign(this.activeEle.style, this.calcSize(size, options.limit))
    this.requestUpdate()
  }

  calcSize (size, limit = {}) {
    let { width, height } = size

    // keep ratio
    if (limit.ratio) {
      let limitHeight
      if (limit.minWidth) width = Math.max(limit.minWidth, width)
      if (limit.maxWidth) width = Math.min(limit.maxWidth, width)

      height = width * limit.ratio

      if (limit.minHeight && height < limit.minHeight) {
        limitHeight = true
        height = limit.minHeight
      }
      if (limit.maxHeight && height > limit.maxHeight) {
        limitHeight = true
        height = limit.maxHeight
      }

      if (limitHeight) {
        width = height / limit.ratio
      }
    } else {
      if (size.width) {
        if (limit.minWidth) width = Math.max(limit.minWidth, width)
        if (limit.maxWidth) width = Math.min(limit.maxWidth, width)
      }
      if (size.height) {
        if (limit.minHeight) height = Math.max(limit.minHeight, height)
        if (limit.maxHeight) height = Math.min(limit.maxHeight, height)
      }
    }

    if (width) size.width = width + 'px'
    if (height) size.height = height + 'px'

    return size
  }

  getNaturalSize () {
    const ele = this.activeEle
    let size = [0, 0]
    if (!ele.getAttribute('data-size')) {
      size = [
        ele.naturalWidth || ele.offsetWidth,
        ele.naturalHeight || ele.offsetHeight
      ]
      ele.setAttribute('data-size', size[0] + ',' + size[1])
    } else {
      size = ele.getAttribute('data-size').split(',')
    }
    return {
      width: parseInt(size[0]),
      height: parseInt(size[1])
    }
  }

  setCursor (value) {
    [document.body, this.activeEle].forEach(el => {
      el.style.cursor = value // eslint-disable-line no-param-reassign
    })
  }
}


/***/ }),

/***/ "./node_modules/quill-resize-module/src/modules/Toolbar.js":
/*!*****************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/modules/Toolbar.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Toolbar)
/* harmony export */ });
/* harmony import */ var _BaseModule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BaseModule */ "./node_modules/quill-resize-module/src/modules/BaseModule.js");
/* harmony import */ var quill_assets_icons_float_left_svg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! quill/assets/icons/float-left.svg */ "./node_modules/quill/assets/icons/float-left.svg");
/* harmony import */ var quill_assets_icons_float_center_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! quill/assets/icons/float-center.svg */ "./node_modules/quill/assets/icons/float-center.svg");
/* harmony import */ var quill_assets_icons_float_right_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! quill/assets/icons/float-right.svg */ "./node_modules/quill/assets/icons/float-right.svg");
/* harmony import */ var quill_assets_icons_float_full_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! quill/assets/icons/float-full.svg */ "./node_modules/quill/assets/icons/float-full.svg");
/* harmony import */ var _assets_pencil_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../assets/pencil.svg */ "./node_modules/quill-resize-module/src/assets/pencil.svg");
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! quill */ "./node_modules/quill/dist/quill.js");
/* harmony import */ var quill__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(quill__WEBPACK_IMPORTED_MODULE_6__);









const Quill = window.Quill || (quill__WEBPACK_IMPORTED_MODULE_6___default())

const Parchment = Quill.import('parchment')

// Quill.js 2.x support
const ClassAttributor = Parchment.ClassAttributor
  ? Parchment.ClassAttributor
  : Parchment.Attributor.Class
const ImageFormatClass = new ClassAttributor('imagestyle', 'ql-resize-style')

class Toolbar extends _BaseModule__WEBPACK_IMPORTED_MODULE_0__["default"] {
  onCreate () {
    // Setup Toolbar
    this.toolbar = document.createElement('div')
    Object.assign(this.toolbar.style, this.options.styles.toolbar)
    this.overlay.appendChild(this.toolbar)

    // Setup Buttons
    this._defineAlignments()
    this._addToolbarButtons()
  }

  _defineAlignments () {
    this.alignments = [
      {
        icon: quill_assets_icons_float_left_svg__WEBPACK_IMPORTED_MODULE_1__["default"],
        apply: () => {
          ImageFormatClass.add(this.activeEle, 'left')
        },
        isApplied: () => ImageFormatClass.value(this.activeEle) === 'left'
      },
      {
        icon: quill_assets_icons_float_center_svg__WEBPACK_IMPORTED_MODULE_2__["default"],
        apply: () => {
          ImageFormatClass.add(this.activeEle, 'center')
        },
        isApplied: () => ImageFormatClass.value(this.activeEle) === 'center'
      },
      {
        icon: quill_assets_icons_float_right_svg__WEBPACK_IMPORTED_MODULE_3__["default"],
        apply: () => {
          ImageFormatClass.add(this.activeEle, 'right')
        },
        isApplied: () => ImageFormatClass.value(this.activeEle) === 'right'
      },
      {
        icon: quill_assets_icons_float_full_svg__WEBPACK_IMPORTED_MODULE_4__["default"],
        apply: () => {
          ImageFormatClass.add(this.activeEle, 'full')
        },
        isApplied: () => ImageFormatClass.value(this.activeEle) === 'full'
      }
    ]
  }

  _addToolbarButtons () {
    const buttons = []
    this.alignments.forEach((alignment, idx) => {
      const button = document.createElement('span')
      buttons.push(button)
      button.innerHTML = alignment.icon
      button.addEventListener('click', () => {
        // deselect all buttons
        buttons.forEach(button => (button.style.filter = ''))
        if (alignment.isApplied()) {
          // If applied, unapply
          ImageFormatClass.remove(this.activeEle)
        } else {
          // otherwise, select button and apply
          this._selectButton(button)
          alignment.apply()
        }
        // image may change position; redraw drag handles
        this.requestUpdate()
      })
      Object.assign(button.style, this.options.styles.toolbarButton)
      if (idx > 0) {
        button.style.borderLeftWidth = '0'
      }
      Object.assign(
        button.children[0].style,
        this.options.styles.toolbarButtonSvg
      )
      if (alignment.isApplied()) {
        // select button if previously applied
        this._selectButton(button)
      }
      this.toolbar.appendChild(button)
    })

    // Edit button
    const button = document.createElement('span')
    button.innerHTML = _assets_pencil_svg__WEBPACK_IMPORTED_MODULE_5__["default"]
    Object.assign(button.style, this.options.styles.toolbarButton)
    button.style.borderLeftWidth = '0'
    button.addEventListener('click', () => {
      this.quill.emitter.emit('resize-edit', this.activeEle, this.blot)
    })
    this.toolbar.appendChild(button)
  }

  _selectButton (button) {
    button.style.filter = 'invert(20%)'
  }
}


/***/ }),

/***/ "./node_modules/quill-resize-module/src/assets/resize.css":
/*!****************************************************************!*\
  !*** ./node_modules/quill-resize-module/src/assets/resize.css ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_resize_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!../../../postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./resize.css */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/quill-resize-module/src/assets/resize.css");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_resize_css__WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_resize_css__WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/js/components/Admin/Post/Update.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/Admin/Post/Update.vue ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Update_vue_vue_type_template_id_6dcc3906_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Update.vue?vue&type=template&id=6dcc3906&scoped=true& */ "./resources/js/components/Admin/Post/Update.vue?vue&type=template&id=6dcc3906&scoped=true&");
/* harmony import */ var _Update_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Update.vue?vue&type=script&lang=js& */ "./resources/js/components/Admin/Post/Update.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Update_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Update_vue_vue_type_template_id_6dcc3906_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _Update_vue_vue_type_template_id_6dcc3906_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "6dcc3906",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Admin/Post/Update.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/Admin/Post/Update.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/Admin/Post/Update.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Update.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Admin/Post/Update.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Admin/Post/Update.vue?vue&type=template&id=6dcc3906&scoped=true&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/components/Admin/Post/Update.vue?vue&type=template&id=6dcc3906&scoped=true& ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_template_id_6dcc3906_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_template_id_6dcc3906_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_template_id_6dcc3906_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Update.vue?vue&type=template&id=6dcc3906&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Admin/Post/Update.vue?vue&type=template&id=6dcc3906&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Admin/Post/Update.vue?vue&type=template&id=6dcc3906&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Admin/Post/Update.vue?vue&type=template&id=6dcc3906&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "el-row",
    [
      _c(
        "el-col",
        [
          _c(
            "el-row",
            { attrs: { type: "flex", justify: "around" } },
            [
              _c(
                "el-col",
                [
                  _c(
                    "el-row",
                    {
                      staticClass: "py-4 mb-5 ps-5 border-bottom",
                      staticStyle: { "background-color": "#e2e8f0" },
                    },
                    [
                      _c(
                        "el-row",
                        [
                          _c(
                            "el-col",
                            [
                              _c(
                                "el-breadcrumb",
                                { attrs: { separator: "/" } },
                                [
                                  _c(
                                    "el-breadcrumb-item",
                                    {
                                      attrs: {
                                        to: { name: "admin.post.index" },
                                      },
                                    },
                                    [_vm._v("Посты")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "el-breadcrumb-item",
                                    {
                                      attrs: {
                                        to: {
                                          name: "admin.post.show",
                                          params: {
                                            id: this.$route.params.id,
                                            slug: this.$route.params.slug,
                                          },
                                        },
                                      },
                                    },
                                    [_vm._v(_vm._s(_vm.post.title))]
                                  ),
                                  _vm._v(" "),
                                  _c("el-breadcrumb-item", [
                                    _vm._v("Редактирование поста"),
                                  ]),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "el-row",
                        { staticClass: "mt-5 mb-5" },
                        [
                          _c("el-col", [
                            _c("h2", [_vm._v("Редактирование поста")]),
                          ]),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form",
                    {
                      ref: "updatePostForm",
                      attrs: { model: _vm.post, rules: _vm.rules },
                    },
                    [
                      _c(
                        "el-row",
                        { attrs: { type: "flex", justify: "between" } },
                        [
                          _c(
                            "el-col",
                            [
                              _c(
                                "el-row",
                                { staticClass: "mt-5" },
                                [
                                  _c("el-col", [
                                    _c("h6", { staticClass: "text-muted" }, [
                                      _vm._v("Заголовок поста"),
                                    ]),
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    { attrs: { span: 10 } },
                                    [
                                      _c(
                                        "el-form-item",
                                        {
                                          attrs: {
                                            prop: "title",
                                            rules: _vm.rules.title,
                                          },
                                        },
                                        [
                                          _c("el-input", {
                                            attrs: {
                                              placeholder:
                                                "Введите заголовок поста",
                                            },
                                            model: {
                                              value: _vm.post.title,
                                              callback: function ($$v) {
                                                _vm.$set(_vm.post, "title", $$v)
                                              },
                                              expression: "post.title",
                                            },
                                          }),
                                        ],
                                        1
                                      ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                { staticClass: "mt-5" },
                                [
                                  _c("el-col", [
                                    _c("h6", { staticClass: "text-muted" }, [
                                      _vm._v("Главное изображение"),
                                    ]),
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    [
                                      _c(
                                        "el-form-item",
                                        [
                                          _c(
                                            "el-upload",
                                            {
                                              attrs: {
                                                action: "#",
                                                "auto-upload": false,
                                                limit: 1,
                                                "list-type": "picture-card",
                                                "on-preview":
                                                  _vm.handlePictureCardPreview,
                                                "on-change":
                                                  _vm.handleAddMainImage,
                                                "on-remove": _vm.handleRemove,
                                              },
                                              model: {
                                                value: _vm.post.main_image,
                                                callback: function ($$v) {
                                                  _vm.$set(
                                                    _vm.post,
                                                    "main_image",
                                                    $$v
                                                  )
                                                },
                                                expression: "post.main_image",
                                              },
                                            },
                                            [
                                              _c("i", {
                                                staticClass: "el-icon-plus",
                                              }),
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "el-dialog",
                                            {
                                              attrs: {
                                                visible: _vm.mainImageIsZoom,
                                              },
                                              on: {
                                                "update:visible": function (
                                                  $event
                                                ) {
                                                  _vm.mainImageIsZoom = $event
                                                },
                                              },
                                            },
                                            [
                                              _c("img", {
                                                attrs: {
                                                  width: "100%",
                                                  src: _vm.mainImageUrl,
                                                  alt: "",
                                                },
                                              }),
                                            ]
                                          ),
                                        ],
                                        1
                                      ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "el-col",
                            [
                              _c(
                                "el-row",
                                { staticClass: "mt-5" },
                                [
                                  _c("el-col", [
                                    _c("h6", { staticClass: "text-muted" }, [
                                      _vm._v("Категория"),
                                    ]),
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    [
                                      _c(
                                        "el-form-item",
                                        {
                                          attrs: {
                                            prop: "category_id",
                                            rules: _vm.rules.category,
                                          },
                                        },
                                        [
                                          _c(
                                            "el-select",
                                            {
                                              attrs: {
                                                clearable: "",
                                                placeholder:
                                                  "Выберите категорию",
                                                filterable: "",
                                              },
                                              model: {
                                                value: _vm.post.category_id,
                                                callback: function ($$v) {
                                                  _vm.$set(
                                                    _vm.post,
                                                    "category_id",
                                                    $$v
                                                  )
                                                },
                                                expression: "post.category_id",
                                              },
                                            },
                                            _vm._l(
                                              _vm.categories,
                                              function (category) {
                                                return _c("el-option", {
                                                  key: category.id,
                                                  attrs: {
                                                    label: category.title,
                                                    value: category.id,
                                                  },
                                                })
                                              }
                                            ),
                                            1
                                          ),
                                        ],
                                        1
                                      ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                {
                                  staticClass: "mt-5",
                                  attrs: {
                                    type: "flex-column",
                                    justify: "start",
                                  },
                                },
                                [
                                  _c("el-col", [
                                    _c("h6", { staticClass: "text-muted" }, [
                                      _vm._v("Теги"),
                                    ]),
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    [
                                      _c(
                                        "el-form-item",
                                        [
                                          _c(
                                            "el-select",
                                            {
                                              attrs: {
                                                multiple: "",
                                                placeholder: "Select",
                                              },
                                              model: {
                                                value: _vm.post.tags_ids,
                                                callback: function ($$v) {
                                                  _vm.$set(
                                                    _vm.post,
                                                    "tags_ids",
                                                    $$v
                                                  )
                                                },
                                                expression: "post.tags_ids",
                                              },
                                            },
                                            _vm._l(_vm.tags, function (tag) {
                                              return _c("el-option", {
                                                key: tag.id,
                                                attrs: {
                                                  label: tag.title,
                                                  value: tag.id,
                                                },
                                              })
                                            }),
                                            1
                                          ),
                                        ],
                                        1
                                      ),
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    { attrs: { span: 9 } },
                                    [
                                      _vm.addTagInputIsVisible
                                        ? _c("el-input", {
                                            ref: "addTagInput",
                                            staticClass: "input-new-tag",
                                            attrs: { size: "mini" },
                                            on: { blur: _vm.handleInputOut },
                                            nativeOn: {
                                              keyup: function ($event) {
                                                if (
                                                  !$event.type.indexOf("key") &&
                                                  _vm._k(
                                                    $event.keyCode,
                                                    "enter",
                                                    13,
                                                    $event.key,
                                                    "Enter"
                                                  )
                                                ) {
                                                  return null
                                                }
                                                return _vm.storeTag({
                                                  title: _vm.tag.title,
                                                  notification:
                                                    _vm.notification,
                                                })
                                              },
                                            },
                                            model: {
                                              value: _vm.tag.title,
                                              callback: function ($$v) {
                                                _vm.$set(_vm.tag, "title", $$v)
                                              },
                                              expression: "tag.title",
                                            },
                                          })
                                        : _c(
                                            "el-button",
                                            {
                                              staticClass: "button-new-tag",
                                              attrs: { size: "small" },
                                              on: {
                                                click: _vm.handleShowInputTag,
                                              },
                                            },
                                            [
                                              _vm._v(
                                                "+ Новый тег\n                                    "
                                              ),
                                            ]
                                          ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "el-row",
                        { staticClass: "mt-5" },
                        [
                          _c("el-col", [
                            _c("h6", { staticClass: "text-muted" }, [
                              _vm._v("Контент превью"),
                            ]),
                          ]),
                          _vm._v(" "),
                          _c(
                            "el-col",
                            { attrs: { span: 20 } },
                            [
                              _c(
                                "el-form-item",
                                {
                                  attrs: {
                                    prop: "content_preview",
                                    rules: _vm.rules.contentPreview,
                                  },
                                },
                                [
                                  _c("vue-editor", {
                                    attrs: {
                                      id: "preview-content-editor",
                                      "editor-toolbar":
                                        _vm.editorSettings
                                          .previewContentEditorToolbar,
                                    },
                                    model: {
                                      value: _vm.post.content_preview,
                                      callback: function ($$v) {
                                        _vm.$set(
                                          _vm.post,
                                          "content_preview",
                                          $$v
                                        )
                                      },
                                      expression: "post.content_preview",
                                    },
                                  }),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "el-row",
                        { staticClass: "mt-5" },
                        [
                          _c("el-col", [
                            _c("h6", { staticClass: "text-muted" }, [
                              _vm._v("Контент"),
                            ]),
                          ]),
                          _vm._v(" "),
                          _c(
                            "el-col",
                            { attrs: { span: 20 } },
                            [
                              _c(
                                "el-form-item",
                                {
                                  attrs: {
                                    prop: "content",
                                    rules: _vm.rules.content,
                                  },
                                },
                                [
                                  _c("vue-editor", {
                                    attrs: {
                                      id: "content-editor",
                                      editorOptions:
                                        _vm.editorSettings.contentEditor,
                                      useCustomImageHandler: "",
                                    },
                                    on: { "image-added": _vm.handleImageAdded },
                                    model: {
                                      value: _vm.post.content,
                                      callback: function ($$v) {
                                        _vm.$set(_vm.post, "content", $$v)
                                      },
                                      expression: "post.content",
                                    },
                                  }),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-row",
                    { staticClass: "mt-5 mb-5" },
                    [
                      _c(
                        "el-col",
                        [
                          _c(
                            "el-button",
                            {
                              attrs: { type: "info", round: "" },
                              on: {
                                click: function ($event) {
                                  _vm.isShowPreview = !_vm.isShowPreview
                                },
                              },
                            },
                            [_vm._v("Предпросмотр\n                        ")]
                          ),
                          _vm._v(" "),
                          _c(
                            "el-button",
                            {
                              attrs: {
                                type: "success",
                                loading: _vm.$store.getters.isLoading,
                                round: "",
                              },
                              on: {
                                click: function ($event) {
                                  return _vm.updatePost("updatePostForm", {
                                    post: _vm.post,
                                    notification: _vm.notification,
                                    router: _vm.$router,
                                    params: _vm.$route.params,
                                  })
                                },
                              },
                            },
                            [_vm._v("Опубликовать\n                        ")]
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-row",
            [
              _c(
                "el-col",
                [
                  _c(
                    "el-dialog",
                    {
                      attrs: { fullscreen: true, visible: _vm.isShowPreview },
                      on: {
                        "update:visible": function ($event) {
                          _vm.isShowPreview = $event
                        },
                      },
                    },
                    [
                      _c(
                        "el-row",
                        { attrs: { type: "flex", justify: "center" } },
                        [
                          _c(
                            "el-col",
                            { attrs: { span: 15 } },
                            [
                              _c(
                                "el-row",
                                { staticClass: "fs-3 mb-5 text-center" },
                                [
                                  _c("el-col", [
                                    _c("div", { staticClass: "text-break" }, [
                                      _vm._v(_vm._s(_vm.post.title)),
                                    ]),
                                  ]),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                {
                                  directives: [
                                    {
                                      name: "show",
                                      rawName: "v-show",
                                      value: _vm.mainImageUrl,
                                      expression: "mainImageUrl",
                                    },
                                  ],
                                },
                                [
                                  _c(
                                    "el-col",
                                    { staticClass: "text-center" },
                                    [
                                      _c("el-image", {
                                        attrs: {
                                          fit: "contain",
                                          src: _vm.mainImageUrl,
                                        },
                                      }),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                [
                                  _c("el-col", [
                                    _c("div", {
                                      staticClass: "ql-editor",
                                      domProps: {
                                        innerHTML: _vm._s(
                                          _vm.post.content_preview
                                        ),
                                      },
                                    }),
                                  ]),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                { staticClass: "mt-3 mb-5" },
                                [
                                  _c("el-col", [
                                    _c("div", {
                                      staticClass: "ql-editor",
                                      domProps: {
                                        innerHTML: _vm._s(_vm.post.content),
                                      },
                                    }),
                                  ]),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);