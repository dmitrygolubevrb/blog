"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_Post_Update_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Post/Update.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Post/Update.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
Object(function webpackMissingModule() { var e = new Error("Cannot find module '../../../lib/notification'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '../../../lib/rules'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
Object(function webpackMissingModule() { var e = new Error("Cannot find module '../../../config/vueEditor'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Create",
  data: function data() {
    return {
      editorSettings: Object(function webpackMissingModule() { var e = new Error("Cannot find module '../../../config/vueEditor'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()),
      isShowPreview: false
    };
  },
  computed: _objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_1__.mapGetters)(['categories', 'tags', 'tag', 'post', 'addTagInputIsVisible'])), {}, {
    mainImageUrl: {
      get: function get() {
        return this.$store.getters.mainImageUrl;
      },
      set: function set(url) {
        this.$store.commit('setMainImageUrl', url);
      }
    },
    mainImageIsZoom: {
      get: function get() {
        return this.$store.getters.mainImageIsZoom;
      },
      set: function set(bool) {
        this.$store.commit('setMainImageIsZoom', bool);
      }
    },
    notification: function notification() {
      return Object(function webpackMissingModule() { var e = new Error("Cannot find module '../../../lib/notification'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(this.$notify);
    },
    rules: function rules() {
      return Object(function webpackMissingModule() { var e = new Error("Cannot find module '../../../lib/rules'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(this.$store.getters.post);
    }
  }),
  mounted: function mounted() {
    this.$store.dispatch('getCategories', this.notification);
    this.$store.dispatch('getTags', this.notification);
    this.$store.dispatch('getAuthUserInfo');
    this.$store.dispatch('getPost', {
      id: this.$route.params.id,
      notification: this.notification
    });
  },
  methods: {
    updatePost: function updatePost(formName, data) {
      var _this = this;

      this.$refs[formName].validate(function (valid) {
        if (valid) _this.$store.dispatch('updatePost', data);
      });
    },
    handleRemove: function handleRemove(file, fileList) {
      this.$store.commit('setMainImage', null);
      this.$store.commit('setMainImageUrl', null);
    },
    handlePictureCardPreview: function handlePictureCardPreview(file) {
      this.$store.commit('setMainImageIsZoom', true);
    },
    handleAddMainImage: function handleAddMainImage(file) {
      this.$store.commit('setMainImageUrl', file.url);
      this.$store.commit('setMainImage', file.raw);
    },
    handleImageAdded: function handleImageAdded(file, Editor, cursorLocation, resetUploader) {
      var _this2 = this;

      var formData = new FormData();
      formData.append('image', file);
      axios.post('/api/posts/images', formData).then(function (res) {
        console.log(res);
        var url = res.data.url;
        Editor.insertEmbed(cursorLocation, 'image', url);
        resetUploader();
      })["catch"](function (error) {
        _this2.$notify({
          type: 'error',
          title: 'Ошибка загрузки',
          text: error.data.message
        });
      });
    },
    storeTag: function storeTag(data) {
      this.$store.dispatch('storeTag', data);
      this.$store.commit('setAddTagInputIsVisible', false);
      this.$store.commit('setTag', {
        id: null,
        title: null
      });
    },
    handleInputOut: function handleInputOut() {
      this.$store.commit('setAddTagInputIsVisible', false);
      this.$store.commit('setTag', {
        id: null,
        title: null
      });
    },
    handleShowInputTag: function handleShowInputTag() {
      var _this3 = this;

      this.$store.commit('setAddTagInputIsVisible', true);
      this.$nextTick(function () {
        _this3.$refs.addTagInput.$refs.input.focus();
      });
    }
  },
  beforeDestroy: function beforeDestroy() {
    this.$store.commit('setPost', {
      id: null,
      title: null,
      content_preview: null,
      content: null,
      category_id: null,
      tags_ids: [],
      main_image: null
    });
  }
});

/***/ }),

/***/ "./resources/js/components/Post/Update.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/Post/Update.vue ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Update_vue_vue_type_template_id_f36bcfc6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Update.vue?vue&type=template&id=f36bcfc6&scoped=true& */ "./resources/js/components/Post/Update.vue?vue&type=template&id=f36bcfc6&scoped=true&");
/* harmony import */ var _Update_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Update.vue?vue&type=script&lang=js& */ "./resources/js/components/Post/Update.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Update_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Update_vue_vue_type_template_id_f36bcfc6_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _Update_vue_vue_type_template_id_f36bcfc6_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "f36bcfc6",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Post/Update.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/Post/Update.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/Post/Update.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Update.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Post/Update.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Post/Update.vue?vue&type=template&id=f36bcfc6&scoped=true&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/Post/Update.vue?vue&type=template&id=f36bcfc6&scoped=true& ***!
  \********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_template_id_f36bcfc6_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_template_id_f36bcfc6_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Update_vue_vue_type_template_id_f36bcfc6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Update.vue?vue&type=template&id=f36bcfc6&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Post/Update.vue?vue&type=template&id=f36bcfc6&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Post/Update.vue?vue&type=template&id=f36bcfc6&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Post/Update.vue?vue&type=template&id=f36bcfc6&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "el-row",
    [
      _c(
        "el-col",
        [
          _c(
            "el-row",
            { attrs: { type: "flex", justify: "around" } },
            [
              _c(
                "el-col",
                [
                  _c(
                    "el-row",
                    {
                      staticClass: "py-4 mb-5 ps-5 border-bottom",
                      staticStyle: { "background-color": "#e2e8f0" },
                    },
                    [
                      _c(
                        "el-row",
                        [
                          _c(
                            "el-col",
                            [
                              _c(
                                "el-breadcrumb",
                                { attrs: { separator: "/" } },
                                [
                                  _c(
                                    "el-breadcrumb-item",
                                    {
                                      attrs: {
                                        to: { name: "admin.post.index" },
                                      },
                                    },
                                    [_vm._v("Посты")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "el-breadcrumb-item",
                                    {
                                      attrs: {
                                        to: {
                                          name: "admin.post.show",
                                          params: {
                                            id: this.$route.params.id,
                                            slug: this.$route.params.slug,
                                          },
                                        },
                                      },
                                    },
                                    [_vm._v(_vm._s(_vm.post.title))]
                                  ),
                                  _vm._v(" "),
                                  _c("el-breadcrumb-item", [
                                    _vm._v("Редактирование поста"),
                                  ]),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "el-row",
                        { staticClass: "mt-5 mb-5" },
                        [
                          _c("el-col", [
                            _c("h2", [_vm._v("Редактирование поста")]),
                          ]),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form",
                    {
                      ref: "updatePostForm",
                      attrs: { model: _vm.post, rules: _vm.rules },
                    },
                    [
                      _c(
                        "el-row",
                        { attrs: { type: "flex", justify: "between" } },
                        [
                          _c(
                            "el-col",
                            [
                              _c(
                                "el-row",
                                { staticClass: "mt-5" },
                                [
                                  _c("el-col", [
                                    _c("h6", { staticClass: "text-muted" }, [
                                      _vm._v("Заголовок поста"),
                                    ]),
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    { attrs: { span: 10 } },
                                    [
                                      _c(
                                        "el-form-item",
                                        {
                                          attrs: {
                                            prop: "title",
                                            rules: _vm.rules.title,
                                          },
                                        },
                                        [
                                          _c("el-input", {
                                            attrs: {
                                              placeholder:
                                                "Введите заголовок поста",
                                            },
                                            model: {
                                              value: _vm.post.title,
                                              callback: function ($$v) {
                                                _vm.$set(_vm.post, "title", $$v)
                                              },
                                              expression: "post.title",
                                            },
                                          }),
                                        ],
                                        1
                                      ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                { staticClass: "mt-5" },
                                [
                                  _c("el-col", [
                                    _c("h6", { staticClass: "text-muted" }, [
                                      _vm._v("Главное изображение"),
                                    ]),
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    [
                                      _c(
                                        "el-form-item",
                                        [
                                          _c(
                                            "el-upload",
                                            {
                                              attrs: {
                                                action: "#",
                                                "auto-upload": false,
                                                limit: 1,
                                                "list-type": "picture-card",
                                                "on-preview":
                                                  _vm.handlePictureCardPreview,
                                                "on-change":
                                                  _vm.handleAddMainImage,
                                                "on-remove": _vm.handleRemove,
                                              },
                                              model: {
                                                value: _vm.post.main_image,
                                                callback: function ($$v) {
                                                  _vm.$set(
                                                    _vm.post,
                                                    "main_image",
                                                    $$v
                                                  )
                                                },
                                                expression: "post.main_image",
                                              },
                                            },
                                            [
                                              _c("i", {
                                                staticClass: "el-icon-plus",
                                              }),
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "el-dialog",
                                            {
                                              attrs: {
                                                visible: _vm.mainImageIsZoom,
                                              },
                                              on: {
                                                "update:visible": function (
                                                  $event
                                                ) {
                                                  _vm.mainImageIsZoom = $event
                                                },
                                              },
                                            },
                                            [
                                              _c("img", {
                                                attrs: {
                                                  width: "100%",
                                                  src: _vm.mainImageUrl,
                                                  alt: "",
                                                },
                                              }),
                                            ]
                                          ),
                                        ],
                                        1
                                      ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "el-col",
                            [
                              _c(
                                "el-row",
                                { staticClass: "mt-5" },
                                [
                                  _c("el-col", [
                                    _c("h6", { staticClass: "text-muted" }, [
                                      _vm._v("Категория"),
                                    ]),
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    [
                                      _c(
                                        "el-form-item",
                                        {
                                          attrs: {
                                            prop: "category_id",
                                            rules: _vm.rules.category,
                                          },
                                        },
                                        [
                                          _c(
                                            "el-select",
                                            {
                                              attrs: {
                                                clearable: "",
                                                placeholder:
                                                  "Выберите категорию",
                                                filterable: "",
                                              },
                                              model: {
                                                value: _vm.post.category_id,
                                                callback: function ($$v) {
                                                  _vm.$set(
                                                    _vm.post,
                                                    "category_id",
                                                    $$v
                                                  )
                                                },
                                                expression: "post.category_id",
                                              },
                                            },
                                            _vm._l(
                                              _vm.categories,
                                              function (category) {
                                                return _c("el-option", {
                                                  key: category.id,
                                                  attrs: {
                                                    label: category.title,
                                                    value: category.id,
                                                  },
                                                })
                                              }
                                            ),
                                            1
                                          ),
                                        ],
                                        1
                                      ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                {
                                  staticClass: "mt-5",
                                  attrs: {
                                    type: "flex-column",
                                    justify: "start",
                                  },
                                },
                                [
                                  _c("el-col", [
                                    _c("h6", { staticClass: "text-muted" }, [
                                      _vm._v("Теги"),
                                    ]),
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    [
                                      _c(
                                        "el-form-item",
                                        {
                                          attrs: {
                                            prop: "tags_ids",
                                            rules: _vm.rules.tagIds,
                                          },
                                        },
                                        [
                                          _c(
                                            "el-select",
                                            {
                                              attrs: {
                                                multiple: "",
                                                placeholder: "Select",
                                              },
                                              model: {
                                                value: _vm.post.tags_ids,
                                                callback: function ($$v) {
                                                  _vm.$set(
                                                    _vm.post,
                                                    "tags_ids",
                                                    $$v
                                                  )
                                                },
                                                expression: "post.tags_ids",
                                              },
                                            },
                                            _vm._l(_vm.tags, function (tag) {
                                              return _c("el-option", {
                                                key: tag.id,
                                                attrs: {
                                                  label: tag.title,
                                                  value: tag.id,
                                                },
                                              })
                                            }),
                                            1
                                          ),
                                        ],
                                        1
                                      ),
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "el-col",
                                    { attrs: { span: 9 } },
                                    [
                                      _vm.addTagInputIsVisible
                                        ? _c("el-input", {
                                            ref: "addTagInput",
                                            staticClass: "input-new-tag",
                                            attrs: { size: "mini" },
                                            on: { blur: _vm.handleInputOut },
                                            nativeOn: {
                                              keyup: function ($event) {
                                                if (
                                                  !$event.type.indexOf("key") &&
                                                  _vm._k(
                                                    $event.keyCode,
                                                    "enter",
                                                    13,
                                                    $event.key,
                                                    "Enter"
                                                  )
                                                ) {
                                                  return null
                                                }
                                                return _vm.storeTag({
                                                  title: _vm.tag.title,
                                                  notification:
                                                    _vm.notification,
                                                })
                                              },
                                            },
                                            model: {
                                              value: _vm.tag.title,
                                              callback: function ($$v) {
                                                _vm.$set(_vm.tag, "title", $$v)
                                              },
                                              expression: "tag.title",
                                            },
                                          })
                                        : _c(
                                            "el-button",
                                            {
                                              staticClass: "button-new-tag",
                                              attrs: { size: "small" },
                                              on: {
                                                click: _vm.handleShowInputTag,
                                              },
                                            },
                                            [
                                              _vm._v(
                                                "+ Новый тег\n                                    "
                                              ),
                                            ]
                                          ),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "el-row",
                        { staticClass: "mt-5" },
                        [
                          _c("el-col", [
                            _c("h6", { staticClass: "text-muted" }, [
                              _vm._v("Контент превью"),
                            ]),
                          ]),
                          _vm._v(" "),
                          _c(
                            "el-col",
                            { attrs: { span: 20 } },
                            [
                              _c(
                                "el-form-item",
                                {
                                  attrs: {
                                    prop: "content_preview",
                                    rules: _vm.rules.contentPreview,
                                  },
                                },
                                [
                                  _c("vue-editor", {
                                    attrs: {
                                      id: "preview-content-editor",
                                      "editor-toolbar":
                                        _vm.editorSettings
                                          .previewContentEditorToolbar,
                                    },
                                    model: {
                                      value: _vm.post.content_preview,
                                      callback: function ($$v) {
                                        _vm.$set(
                                          _vm.post,
                                          "content_preview",
                                          $$v
                                        )
                                      },
                                      expression: "post.content_preview",
                                    },
                                  }),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "el-row",
                        { staticClass: "mt-5" },
                        [
                          _c("el-col", [
                            _c("h6", { staticClass: "text-muted" }, [
                              _vm._v("Контент"),
                            ]),
                          ]),
                          _vm._v(" "),
                          _c(
                            "el-col",
                            { attrs: { span: 20 } },
                            [
                              _c(
                                "el-form-item",
                                {
                                  attrs: {
                                    prop: "content",
                                    rules: _vm.rules.content,
                                  },
                                },
                                [
                                  _c("vue-editor", {
                                    attrs: {
                                      id: "content-editor",
                                      editorOptions:
                                        _vm.editorSettings.contentEditor,
                                      useCustomImageHandler: "",
                                    },
                                    on: { "image-added": _vm.handleImageAdded },
                                    model: {
                                      value: _vm.post.content,
                                      callback: function ($$v) {
                                        _vm.$set(_vm.post, "content", $$v)
                                      },
                                      expression: "post.content",
                                    },
                                  }),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-row",
                    { staticClass: "mt-5 mb-5" },
                    [
                      _c(
                        "el-col",
                        [
                          _c(
                            "el-button",
                            {
                              attrs: { type: "info", round: "" },
                              on: {
                                click: function ($event) {
                                  _vm.isShowPreview = !_vm.isShowPreview
                                },
                              },
                            },
                            [_vm._v("Предпросмотр\n                        ")]
                          ),
                          _vm._v(" "),
                          _c(
                            "el-button",
                            {
                              attrs: {
                                type: "success",
                                loading: _vm.$store.getters.isLoading,
                                round: "",
                              },
                              on: {
                                click: function ($event) {
                                  return _vm.updatePost("updatePostForm", {
                                    post: _vm.post,
                                    notification: _vm.notification,
                                  })
                                },
                              },
                            },
                            [_vm._v("Опубликовать\n                        ")]
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-row",
            [
              _c(
                "el-col",
                [
                  _c(
                    "el-dialog",
                    {
                      attrs: { fullscreen: true, visible: _vm.isShowPreview },
                      on: {
                        "update:visible": function ($event) {
                          _vm.isShowPreview = $event
                        },
                      },
                    },
                    [
                      _c(
                        "el-row",
                        { attrs: { type: "flex", justify: "center" } },
                        [
                          _c(
                            "el-col",
                            { attrs: { span: 15 } },
                            [
                              _c(
                                "el-row",
                                { staticClass: "fs-3 mb-5 text-center" },
                                [
                                  _c("el-col", [
                                    _c("div", { staticClass: "text-break" }, [
                                      _vm._v(_vm._s(_vm.post.title)),
                                    ]),
                                  ]),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                {
                                  directives: [
                                    {
                                      name: "show",
                                      rawName: "v-show",
                                      value: _vm.mainImageUrl,
                                      expression: "mainImageUrl",
                                    },
                                  ],
                                },
                                [
                                  _c(
                                    "el-col",
                                    { staticClass: "text-center" },
                                    [
                                      _c("el-image", {
                                        attrs: {
                                          fit: "contain",
                                          src: _vm.mainImageUrl,
                                        },
                                      }),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                [
                                  _c("el-col", [
                                    _c("div", {
                                      staticClass: "ql-editor",
                                      domProps: {
                                        innerHTML: _vm._s(
                                          _vm.post.content_preview
                                        ),
                                      },
                                    }),
                                  ]),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-row",
                                { staticClass: "mt-3 mb-5" },
                                [
                                  _c("el-col", [
                                    _c("div", {
                                      staticClass: "ql-editor",
                                      domProps: {
                                        innerHTML: _vm._s(_vm.post.content),
                                      },
                                    }),
                                  ]),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);