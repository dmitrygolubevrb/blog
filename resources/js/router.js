import Vue from "vue";
import VueRouter from "vue-router";


Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',

    routes: [

        // Auth Routes
        {
            path: '/auth/login',
            component: () => import('./components/Auth/Login'),
            name: 'auth.login'
        },
        {
            path: '/auth/register',
            component: () => import('./components/Auth/Registration'),
            name: 'auth.register'
        },
        {
            path: '/auth/forgot-password',
            component: () => import('./components/Auth/ForgotPassword'),
            name: 'auth.forgot'
        },
        {
            path: '/auth/reset-password',
            component: () => import('./components/Auth/ResetPassword'),
            name: 'auth.reset-password'
        },
        {
            path: '/auth/email/verify',
            component: () => import('./components/Auth/VerifyEmail'),
            name: 'auth.verify-email'
        },

        // Admin Routes

        // {
        //     path: '/admin',
        //     component: () => import('./components/Admin/Index'),
        //     name: 'admin.index'
        // },
        {
            path: '/admin/categories',
            component: () => import('./components/Admin/Category/Index'),
            name: 'admin.category.index'
        },
        {
            path: '/admin/tags',
            component: () => import('./components/Admin/Tag/Index'),
            name: 'admin.tag.index'
        },
        {
            path: '/admin/users',
            component: () => import('./components/Admin/User/Index'),
            name: 'admin.user.index'
        },
        {
            path: '/admin/posts',
            component: () => import('./components/Admin/Post/Index'),
            name: 'admin.post.index'
        },
        {
            path: '/admin/posts/create',
            component: () => import('./components/Post/Create'),
            name: 'admin.post.create'
        },
        {
            path: '/admin/posts/:id-:slug',
            component: () => import('./components/Admin/Post/Show'),
            name: 'admin.post.show'
        },
        {
            path: '/admin/posts/:id-:slug/edit',
            component: () => import('./components/Admin/Post/Update'),
            name: 'admin.post.update'
        },
        {
            path: '/admin/users/create',
            component: () => import('./components/Admin/User/Create'),
            name: 'admin.user.create'
        },

        // Main Routes
        {
            path: '/',
            component: () => import('./components/Main/Index'),
            name: 'main.index'
        },
        {
            path: '/blog',
            component: () => import('./components/Blog/Index'),
            name: 'blog.index'
        },
        {
            path: '/posts/create',
            component: () => import('./components/Post/Create'),
            name: 'post.create'
        },
        {
            path: '/posts/:slug',
            component: () => import('./components/Post/Show'),
            name: 'post.show'
        },
        {
            path: '/categories/:slug',
            component: () => import('./components/Category/Post/Index'),
            name: 'category.post.index'
        }
    ]
})

router.beforeEach((to, from ,next) => {
    const token = localStorage.getItem('xsrf_token')
    if(!token){
        if(to.name === 'auth.login' || to.name === 'auth.register') return next()
        return next()
    }
    next()
})

export default router
