import Model from "./Model";

export default class User extends Model {

    resource() {
        return 'users';
    }

    baseURL() {
        return 'http://127.0.0.1:8000/api/admin';
    }
}
