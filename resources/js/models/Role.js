import Model from "./Model";

export default class Role extends Model {

    resource() {
        return 'roles';
    }

    baseURL() {
        return 'http://127.0.0.1:8000/api/admin';
    }

    request(config){
        return this.$http.request(config)
    }


}
