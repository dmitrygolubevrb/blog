import Category from "../../models/Category";

const state = {
    categories: null,
    category: {
        id: null,
        title: null
    },
    addCategoryInputIsVisible: false,
    editCategoryId: null,
}

const getters = {
    category: () => state.category,
    categories: () => state.categories,
    editCategoryId: () => state.editCategoryId,
    addCategoryInputIsVisible: () => state.addCategoryInputIsVisible
}

const mutations = {
    setCategory(state, category) {
        state.category = category
    },
    setCategories(state, categories) {
        state.categories = categories
    },
    setEditCategoryId(state, id) {
        state.editCategoryId = id
    },
    setAddCategoryInputIsVisible(state, bool) {
        state.addCategoryInputIsVisible = bool
    }
}

const actions = {
    storeCategory({commit, dispatch}, data) {
        const category = new Category({title: data.title})
        category.save().then(() => {
            dispatch('getCategories', data.notification)
                .then(() => commit('setCategory', {id: null, title: null}))
                .then(() => data.notification.success(`Категория ${data.title} успешно создана`))
        }).catch(error => {
            data.notification.error(error.response.data.message)
        })
    },

    destroyCategory({dispatch}, data) {
        const category = new Category({id: data.category.id})
        category.delete().then(() => {
            dispatch('getCategories', data.notification)
                .then(() => data.notification.success(`Категория ${data.category.title} успешно удалена`))
        }).catch(error => {
            data.notification.error(error.response.data.message)
        })
    },

    updateCategory({dispatch, commit}, data) {
        const category = new Category({id: data.id})
        category.id = data.category.id
        category.title = data.category.title
        category.patch().then(() => {
            dispatch('getCategories', data.notification)
                .then(() => commit('setEditCategoryId', null))
                .then(() => data.notification.success(`Категория успешно обновлена`))
        }).catch(error => {
            data.notification.error(error.response.data.message)
        })

    },

    getCategories({commit}, notification) {
        new Category().get().then(res => {
            commit('setCategories', res.data)
        }).catch(error => {
            notification.error(error.response.data.message)
        })
    },

    changeEditCategoryId({commit, state}, category) {
        commit('setAddCategoryInputIsVisible', false)
        commit('setEditCategoryId', category.id)
        commit('setCategory', category)
    },
    unsetEditCategoryId({commit}) {
        commit('setEditCategoryId', null)
        commit('setCategory', {id: null, title: null})
    },
    toggleAddCategoryInputIsVisible({commit}, bool) {
        commit('setCategory', {id: null, title: null})
        commit('setEditCategoryId', null)
        commit('setAddCategoryInputIsVisible', bool)
    }
}

export default {
    state, mutations, getters, actions
}
