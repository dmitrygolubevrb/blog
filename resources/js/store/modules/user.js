import User from "../../models/User";
import Role from '../../models/Role'

const state = {
    users: null,
    user: {
        id: null,
        name: null,
        email: null,
        role: 1
    },
    isEdit: false,
    roles: null,
}

const getters = {
    users: () => state.users,
    user: () => state.user,
    isEdit: () => state.isEdit,
    roles: () => state.roles,
}

const mutations = {
    setUsers(state, users) {
        state.users = users
    },
    setUser(state, user) {
        state.user = user
    },
    setIsEdit(state, bool) {
        state.isEdit = bool
    },
    setRoles(state, roles) {
        state.roles = roles
    },

}

const actions = {

    getUsers({commit}) {
        new User().get().then(res => {
            commit('setUsers', res.data)
        })
    },

    getSearchUsers({commit}, data){
        new User().where('email', data.searchUserQuery).get().then(res => {
              commit('setUsers', res.data)
          }).catch(error => {
              data.notification.error(error.response.data.message)
          })
    },

    storeUser({commit, dispatch}, data) {
        commit('toggleIsLoading')
        new User({email: data.user.email, name: data.user.name, role: data.user.role})
            .save()
            .then(() => {
                data.notification.success(`Пользователь ${data.user.email} успешно создан`)
                data.router.push({name: 'admin.user.index'})
            })
            .catch(error => {
                data.notification.error(error.response.data.message)
            })
            .finally(() => commit('toggleIsLoading'))
    },

    destroyUser({dispatch}, data) {
        new User({id: data.id}).delete().then(res => {
            dispatch('getUsers')
            data.notification.success('Пользователь помещен в корзину')
        }).catch(error => {
            data.notification.error(error.response.data.message)
        })
    },

    updateUser({dispatch, commit, getters}, data) {
        commit('toggleIsLoading')
        const user = new User({id: data.user.id})
        user.name = data.user.name
        user.email = data.user.email
        user.role = data.user.role
        user.patch().then(() => {
            dispatch('getUsers')
                .then(() => commit('setIsEdit', false))
                .then(() => data.notification.success(`Пользователь ${data.user.email} успешно отредактирован`))
        }).catch(error => {
            data.notification.error(error.response.data.message)
        }).finally(() => commit('toggleIsLoading'))
    },

    getAuthUser({commit}){

    },
    getRoles({commit}, notification) {
        new Role().get().then(res => {
            commit('setRoles', res[0].roles)
        }).catch(error => {
            notification.error(error.response.data.message)
        })
    }
}

export default {
    state, mutations, getters, actions
}
