const state = {
    registrationFields: {
        name: null,
        email: null,
        password: null,
        password_confirmation: null
    },
    loginFields: {
        email: null,
        password: null
    },
    forgotPasswordFields: {
        email: null
    },
    resetPasswordFields: {
        email: null,
        password: null,
        password_confirmation: null,
        token: null
    },
    resetPasswordLinkIsSend: false,
    authUserInfo: {
        email: null,
        name: null,
        email_verified: null
    }
}

const getters = {
    REGISTRATION_FIELDS: () => state.registrationFields,
    LOGIN_FIELDS: () => state.loginFields,
    FORGOT_PASSWORD_FIELDS: () => state.forgotPasswordFields,
    RESET_PASSWORD_FIELDS: () => state.resetPasswordFields,
    RESET_PASSWORD_LINK_IS_SEND: () => state.resetPasswordLinkIsSend,
    AUTH_USER_INFO: () => state.authUserInfo
}

const mutations = {
    SET_REGISTRATION_FIELDS(state, payload) {
        state.registrationFields = payload
    },
    SET_LOGIN_FIELDS(state, payload) {
        state.loginFields = payload
    },
    SET_FORGOT_PASSWORD_FIELDS(state, payload) {
        state.forgotPasswordFields = payload
    },
    SET_RESET_PASSWORD_FIELDS(state, payload) {
        state.resetPasswordFields = payload
    },
    SET_RESET_PASSWORD_LINK_IS_SEND(state, bool){
        state.resetPasswordLinkIsSend = bool
    },
    SET_AUTH_USER_INFO(state, payload){
        state.authUserInfo = payload
    }
}

const actions = {

    REGISTRATION({commit}, payload) {
        axios.get('/sanctum/csrf-cookie').then(() => {
            axios.post('/register', payload.registrationFields)
                .then(res => {
                    payload.router.push({name: 'auth.verify-email'})
                })
                .catch(error => {
                    payload.notification.error(error.response.data.message)
                })
        })
    },

    LOGIN({commit, getters}, payload) {
        axios.get('/sanctum/csrf-cookie')
            .then(res => {
                axios.post('/login', payload.loginFields)
                    .then(res => {
                        localStorage.setItem('xsrf_token', res.config.headers['X-XSRF-TOKEN'])
                        window.location.replace('/blog')
                    })
                    .catch(error => {
                        commit('SET_LOGIN_FIELDS', {email: null, password: null})
                        payload.notification.error(error.response.data.message)
                    })
                    .finally(() => commit('toggleIsLoading'))
            })
    },

    SEND_RESET_PASSWORD_LINK({commit}, data) {
        commit('toggleIsLoading')
        axios.get('/sanctum/csrf-cookie').then(() => {
            axios.post('/forgot-password', data.forgotPasswordFields)
                .then(res => {
                    commit('setResetPasswordLinkIsSend', true)
                    data.notification.success(res.data.message)
                })
                .catch(error => {
                    commit('setResetPasswordLinkIsSend', false)
                    data.notification.error(error.response.data.message)
                })
                .finally(() => commit('toggleIsLoading'))
        })
    },

    RESEND_EMAIL_VERIFY_LINK({}, notification) {
        axios.get('/sanctum/csrf-cookie').then(() => {
            axios.post('/email/verification-notification')
                .then(() => {
                    notification.success('Ссылка была отправлена еще раз')
                })
                .catch(error => {
                    notification.error(error.response.data.message)
                })
        })
    },

    RESET_PASSWORD({commit}, data) {
        axios.get('/sanctum/csrf-cookie').then(() => {
            axios.post('/reset-password', data.resetPasswordFields)
                .then(res => {
                    data.notification.success(res.data.message)
                    data.router.push({name: 'auth.login'})
                })
                .catch(error => {
                    data.notification.error(error.response.data.message)
                })

        })
    },
    LOGOUT({}) {
        axios.post('/logout').then(() => {
            localStorage.removeItem('xsrf_token')
            window.location.replace('/auth/login')
        })
    },
    GET_AUTH_USER_INFO({commit}){
        axios.get('/api/user').then(res => {
            commit('SET_AUTH_USER_INFO', {
                email: res.data.data.email,
                name: res.data.data.name,
                email_verified: res.data.data.email_verified_at
            })
        })
    }

}

export default {
    state, mutations, getters, actions
}
