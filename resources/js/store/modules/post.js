import Post from "../../models/Post";
import Blog from "../../models/Blog";
import notification from "../../lib/notification";

const state = {
    posts: null,
    blogPosts: null,
    post: {
        id: null,
        title: null,
        content_preview: null,
        content: null,
        category_id: null,
        tags_ids: [],
        main_image: null,
        image: {
            url: null,
            preview_url: null
        }
    },
    mainImageUrl: null,
    mainImageIsZoom: false,

}

const getters = {
    posts: () => state.posts,
    blogPosts: () => state.blogPosts,
    post: () => state.post,
    mainImageUrl: () => state.mainImageUrl,
    mainImageIsZoom: () => state.mainImageIsZoom,
}

const mutations = {
    setPosts(state, posts) {
        state.posts = posts
    },
    setBlogPosts(state, blogPosts) {
        state.blogPosts = blogPosts
    },
    setPost(state, post) {
        state.post = post
    },
    setMainImage(state, mainImage) {
        state.post.main_image = mainImage
    },
    setMainImageUrl(state, mainImageUrl) {
        state.mainImageUrl = mainImageUrl
    },
    setMainImageIsZoom(state, bool) {
        state.mainImageIsZoom = bool
    },

}

const actions = {
    getPosts({state, commit}) {
        new Post().orderBy('-created_at').get().then(res => {
            commit('setPosts', res.data)
        })
    },
    getBlogPosts({commit}, notification) {
        new Blog().get().then(posts => {
            commit('setPosts', posts[0])
        }).catch(error => {
            notification.error(error.response.data.message)
        })
    },
    getSearchPosts({commit}, data) {
        new Post().where('title', query).get().then(res => {
            console.log(res);
        }).catch(error => {
            console.log(error.response);
        })
    },
    getCategoryPosts({state, commit}, category) {
        axios.get(`/api/categories/${category}/posts`).then(res => {
            console.log(res);
        }).catch(error => {
            console.log(error.response);
        })
    },
    getPost({state, commit}, data) {
        Post.find(data.id)
            .then( res => {
                console.log(res);
                commit('setMainImageUrl', res.data.image.url)
                commit('setPost', {
                    id: res.data.id,
                    author: res.data.author.name,
                    title: res.data.title,
                    content_preview: res.data.content_preview,
                    content: res.data.content,
                    category_id: res.data.category.id,
                    tags_ids: res.data.tags.map(item => {
                        return item.id
                    }),
                    image: res.data.image,
                    created_at: res.data.created_at,
                    comments_count: res.data.comments_count,
                    likes_count: res.data.likes_count,
                    view_count: res.data.view_count
                })
            })
            .catch(error => {
                console.log(error)
            })
    },
    storePost({commit, dispatch}, data) {
        commit('toggleIsLoading')
        const post = new Post({
            title: data.post.title,
            content_preview: data.post.content_preview,
            content: data.post.content,
            category_id: data.post.category_id,
            tags_ids: data.post.tags_ids,
            main_image: data.post.main_image,
        })
        post.save().then(res => {
            dispatch('getPosts')
            data.router.push({name: 'admin.post.index'})
            data.notification.success('Пост успешно создан')
        }).catch(error => {
            data.notification.error(error.response.data.message)
        }).finally((() => commit('toggleIsLoading')))
    },
    updatePost({}, data){

        const post = new Post({id: data.post.id})
        post._method = 'PATCH'
        post.title = data.post.title
        post.content_preview = data.post.content_preview
        post.content = data.post.content
        post.category_id = data.post.category_id
        post.tags_ids = data.post.tags_ids
        post.main_image = data.post.main_image
        post.config({method: 'POST'}).save().then(res => {
            data.router.push({name: 'admin.post.show', params: data.params})
        }).catch(error => data.notification.error(error.response.data.message))
    },

    destroyPost({dispatch}, data) {
        console.log(data)
        new Post({id: data.post.id}).delete().then(res => {
            dispatch('getPosts')
            data.notification.success('Пост успешно удален')
        }).catch(error => {
            data.notification.error(error.response.data.message)
            console.log(error.response);
        })
    }
}

export default {
    state, mutations, getters, actions
}
