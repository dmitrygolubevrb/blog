
const state = {
    currentPage: null,
    itemsPerPage: null,
    paginatedData: null,
    countItems: null
}

const getters = {
    currentPage: () => state.currentPage,
    itemsPerPage: () => state.itemsPerPage,
    paginatedData: () => state.paginatedData,
    countItems: () => state.countItems
}

const mutations = {
    setCurrentPage(state, currentPage) {
        state.currentPage = currentPage
    },
    setItemsPerPage(state, itemsPerPage) {
        state.itemsPerPage = itemsPerPage
    },
    setPaginatedData(state, paginatedData) {
        state.paginatedData = paginatedData
    },
    setCountItems(state, countItems) {
        state.countItems = countItems
    },
}

const actions = {
    setPaginatedData({commit, getters}, data) {
        commit('setCountItems', data.data.length)
        let currentPage = data.page ? data.page : getters.currentPage ? getters.currentPage : 1,
            start = (currentPage - 1) * getters.itemsPerPage,
            end = start + getters.itemsPerPage
        commit('setCurrentPage', currentPage)
        commit('setPaginatedData', data.data.slice(start, end))
    }
}

export default {
    state, mutations, getters, actions
}
