import Tag from "../../models/Tag";

const state = {
    tags: null,
    tag: {
        id: null,
        title: null
    },
    addTagInputIsVisible: false,
    editTagId: null,
}

const getters = {
    tag: () => state.tag,
    tags: () => state.tags,
    editTagId: () => state.editTagId,
    addTagInputIsVisible: () => state.addTagInputIsVisible
}

const mutations = {
    setTag(state, tag) {
        state.tag = tag
    },
    setTags(state, tags) {
        state.tags = tags
    },
    setEditTagId(state, id) {
        state.editTagId = id
    },
    setAddTagInputIsVisible(state, bool) {
        state.addTagInputIsVisible = bool
    }
}

const actions = {
    storeTag({commit, dispatch}, data) {
        const tag = new Tag({title: data.title})
        tag.save().then(() => {
            dispatch('getTags', data.notification)
                .then(() => commit('setTag', {id: null, title: null}))
                .then(() => data.notification.success(`Тег ${data.title} успешно создан`))
        }).catch(error => {
            data.notification.error(error.response.data.message)
        })
    },

    destroyTag({dispatch}, data) {
        const tag = new Tag({id: data.tag.id})
        tag.delete().then(() => {
            dispatch('getTags', data.notification)
                .then(() => data.notification.success(`Тег ${data.tag.title} успешно удален`))
        }).catch(error => {
            data.notification.error(error.response.data.message)
        })
    },

    updateTag({dispatch, commit}, data) {
        const tag = new Tag({id: data.id})
        tag.id = data.tag.id
        tag.title = data.tag.title
        tag.patch().then(() => {
            dispatch('getTags', data.notification)
                .then(() => commit('setEditTagId', null))
                .then(() => data.notification.success(`Тег успешно обновлен`))
        }).catch(error => {
            data.notification.error(error.response.data.message)
        })
    },

    getTags({commit}, notification) {
        new Tag().get().then(res => {
            commit('setTags', res.data)
        }).catch(error => {
            notification.error(error.response.data.message)
        })
    },

    changeEditTagId({commit, state}, tag) {
        commit('setAddTagInputIsVisible', false)
        commit('setEditTagId', tag.id)
        commit('setTag', tag)
    },
    unsetEditTagId({commit}) {
        commit('setEditTagId', null)
        commit('setTag', {id: null, title: null})
    },
    toggleAddTagInputIsVisible({commit}, bool) {
        commit('setTag', {id: null, title: null})
        commit('setEditTagId', null)
        commit('setAddTagInputIsVisible', bool)
    }
}

export default {
    state, mutations, getters, actions
}
