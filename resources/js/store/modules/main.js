const state = {
    isLoading: false,
}

const getters = {
    IS_LOADING: () => state.isLoading,

}

const mutations = {
    SET_IS_LOADING(state, bool) {
        state.isLoading = bool
    },

}

const actions = {
    SET_IS_LOADING: ({commit}, bool) => {
        commit('SET_IS_LOADING', bool)
}
}

export default {
    state, mutations, getters, actions
}
