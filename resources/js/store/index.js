import Vue from "vue";
import Vuex from 'vuex';
import Post from './modules/post'
import Category from './modules/category'
import User from './modules/user'
import Tag from './modules/tag'
import Auth from './modules/auth'
import Main from './modules/main'
import Paginator from './modules/paginator'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        Post,
        Category,
        User,
        Tag,
        Auth,
        Main,
        Paginator
    },
})
