export default function notification(notificator)
{
    if(notificator.name === 'Message') return {
        error: (message) => {
            return notificator({
                showClose: true,
                message: message,
                type: 'error'
            })
        },
        success: (message) => {
            return notificator({
                showClose: true,
                message: message,
                type: 'success'
            })
        },
        warning: (message) => {
            return notificator({
                showClose: true,
                message: message,
                type: 'warning'
            })
        }
    }
    if(notificator.name === 'Notification') return {
        error: (message) => {
            return notificator({
                title: 'Ошибка',
                message: message,
                type: 'error',
                position: 'bottom-right'
            })
        },
        success: (message) => {
            return notificator({
                title: 'Успешно',
                message: message,
                type: 'success',
                position: 'bottom-right'
            })
        },
        warning: (message) => {
            return notificator({
                title: 'Предупреждение',
                message: message,
                type: 'warning',
                position: 'bottom-right'
            })
        }
    }
}
