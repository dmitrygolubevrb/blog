export default function (formData) {
    let validatePasswordConfirmed = (rule, value, callback) => {
        if (value === '' || value === null) callback(new Error('Повторите пароль'))
        else if (value !== formData.password && value !== null) callback(new Error('Пароли должны совпадать'))
        callback()
    }
    let validatePassword = (rule, value, callback) => {
        if (value === '' || value === null) callback(new Error('Введите пароль'))
        else if (value !== formData.password_confirmation && formData.password_confirmation !== undefined && formData.password_confirmation !== null) callback(new Error('Пароли должны совпадать'))
        callback()
    }
    return {
        password: [
            {validator: validatePassword, trigger: 'blur'},
        ],
        password_confirmation: [
            {validator: validatePasswordConfirmed, trigger: 'blur'},
        ],
        email: [
            {required: true, message: 'Введите email', trigger: 'blur'},
            {type: 'email', message: 'Введите корректный email', trigger: 'blur'}
        ],
        name: [
            {required: true, message: 'Введите имя', trigger: 'blur'},
        ],
        title: [
            {required: true, message: 'Введите заголовок поста', trigger: 'blur'}
        ],
        image: [
            {required: true, message: 'Необходимо выбрать изображение', trigger: 'blur'}
        ],
        contentPreview: [
            {required: true, message: 'Необходимо указать превью', trigger: 'blur'}
        ],
        content: [
            {required: true, message: 'Необходимо указать контент', trigger: 'blur'}
        ],
        category: [
            {required: true, message: 'Необходимо выбрать категорию', trigger: 'blur'}
        ],
        tagIds: [
            {required: true, message: 'Выберите хотя бы один тег', trigger: 'blur'}
        ]
    }

}
