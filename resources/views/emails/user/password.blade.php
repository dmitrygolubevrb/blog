@component('mail::message')

Ваш пароль: {{ $password }}

@component('mail::button', ['url' => 'http://127.0.0.1:8000/auth/login'])
Войти
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
